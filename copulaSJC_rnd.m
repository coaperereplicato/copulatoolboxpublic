function rnd = copulaSJC_rnd(tauL, tauU, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function draws sample from (Symmetric) Joe Clayton copula
% Args:
%   - tauU = N * 1 vector, or scalar, of JC copula 'upper' parameter
%   - tauL = N * 1 vector, or scalar, of JC copula 'lower' parameter
%   - 'numSim' = (optional) integer; number of draws; defaults to
%   length(tauX) if tauX is a vector (here X = U,L)
%   - 'seed' = (optional) nonnegative scalar integer with which to 
%   initialize all streams (this fn default = 'shuffle' which creates seed
%   based on the current time)
%   - 'copula' = 'JoeClayton' or 'SymmetricJoeClayton' (default)
%   - 'useParallel' = (optional) logical, false (default) => serial 
%   (nonsequential) processing; true => parallel processing. Nb: (i) set 
%   default to false to avoid initialising parallel pool for low
%   dimensional simulation calls; (ii) max no. workers set to 12 in code.
% Output:
%   - rnd = 2 * N (or 2 * numSim) matrix containng sample [u, v] 
% Nb:
%   - Function is not properly tested (ex. against Patton's SJC rnd)
%   - If {tauU, tauL} are scalars and numSim > 1, then every simulation
%   uses the same (unique) {tauU, tauL}. If {tauU, tauL} are vectors then
%   then numSim reverts to length(tauU) = length(tauL) and each simulation 
%   uses a different element in the numSim * 2 matrix, [tauU, tauL].
%   - Pseudo code:
%       1. draw v ~ U(0,1) w/ rand()
%       2. draw t ~ U(0,1) w/ rand()
%       3. solve for u where t = C_v(u)
%       4. return (u,v)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create input parser object 
p = inputParser;

% Default number of draws
defaultNumSim = 1;
checkNumSim = @(x) ~mod(x, 1);   % check if integer

% Default seed
defaultSeed = 'shuffle';         % used to set global stream according current time

% Default copula
defaultCopula = 'SymmetricJoeClayton'; 
validCopulas ={'JoeClayton', 'SymmetricJoeClayton'};
checkCopulas = @(x) any(validatestring(x, validCopulas));

% Default setting for parallel processing
defaultParallel = false;     % false for serial (non-sequential) processing, true for parallel; default to false to avoid initialising parallel pool for single rv draws

% Add inputs to scheme
addRequired(p, 'tauU', @isnumeric);
addRequired(p, 'tauL', @isnumeric);
addOptional(p, 'numSim', defaultNumSim, checkNumSim)
addOptional(p, 'seed', defaultSeed)
addOptional(p, 'copula', defaultCopula, checkCopulas)
addOptional(p, 'useParallel', defaultParallel, @islogical)

% Parse (requiring exact matches)
p.KeepUnmatched = false;
parse(p, tauL, tauU, varargin{:})

% Get optional inputs
numSim = p.Results.numSim;
seed = p.Results.seed;
copula = p.Results.copula;
useParallel = p.Results.useParallel;

% Set seed (global random stream according to user specified seed or default current time); ref.: https://au.mathworks.com/help/matlab/ref/randstream.randstream.html
s = RandStream('mt19937ar', 'Seed', seed); 
RandStream.setGlobalStream(s);

% Check dimensions
if sum(size(tauU) == size(tauL)) ~= 2
    error('tauU and tauL must have the same dimensions')
elseif sum(size(tauU)) ~= 2
    % Set numSim equal to length(tauU) = length(tauL) if tauU/tauL is a vector
    numSim = length(tauU);
    sprintf('warning: numSim adjusted')
end

% Resize tauU and tauL if scalar and numSim > 1
if max(size(tauU)) ~= numSim        % tauU sufficies as we require tauU and tauL have same dimensions
    tauU = tauU .* ones(numSim, 1);
    tauL = tauL .* ones(numSim, 1);
end

% Draw
rnd = - 999.99 * ones(numSim, 2); % preallocate for simulated [u, v]
rnd(:, 2) = rand(numSim, 1);      % simulate v
t = rand(numSim, 1);              % simulate intermediate U(0,1) indepednent of v   

% Partial derivative of Joe-Clayton copula w.r.t. 'v' (equivalent to 
% Pr(U <= u | V = v)). See ex. Nelsen (2006), An introduction to copulas,
% p. 41. Algebra comes from /LandEconomy/Mathematica/JC_copula.nb
JCcdfUgivenV = @(u, v, k, g) (1 - v).^(k - 1) ...
    .* (1 - (1 - v).^k).^(- g - 1) ...
    .* (1 - ((1 - (1 - u).^k).^(- g) + (1 - (1 - v).^k).^(- g) - 1).^(- 1 ./ g)).^(1 ./ k - 1) ...
    .* ((1 - (1 - u).^k).^(- g) + (1 - (1 - v).^k).^(- g) - 1).^(- 1 ./ g - 1);

% Get conditional copula function depending on user copula specification
if strcmp(copula, 'SymmetricJoeClayton')
    condCopula = @(u, v, k1, g1, k2, g2) 0.5 * (JCcdfUgivenV(u, v, k1, g1) -  JCcdfUgivenV(1 - u, 1 - v, k2, g2) + 1); % symmetric JC condtional copula cf. /LandEconomy/Mathematica/JC_copula.nb
    rootEqn = @(u, v, k1, g1, k2, g2, t) condCopula(u, v, k1, g1, k2, g2) - t;
else
    condCopula = JCcdfUgivenV;                 % regular Joe-Clayton copula
    rootEqn = @(u, v, k, g, t) condCopula(u, v, k, g) - t;
end

% Transformed parameters
k = @(tau) 1 ./ log2(2 - tau);
g = @(tau) - 1 ./ log2(tau); 

% Set workers to emulate serial or parallel processing
if useParallel
    maxNumWorkers = 12;    % parallel; 12 = default max number of workers
else
    maxNumWorkers = 0;     % serial; parfor will execute serially but order need not be sequential (as would be the case w/ for loop)
end

% Simulate  u (given v)
rndTemp = rnd(:, 1); % temporary variable needed to avoid issues in parfor loop
parfor (ii = 1:1:numSim, maxNumWorkers)
    if strcmp(copula, 'SymmetricJoeClayton')
        % This equation needs to be solved for u (i.e. v satisfies t = C_v(u) where
        % C_v(u) := SJCcdfUgivenV(u, v, ., .) and t ~ U(0,1))
        rootEqn2 = @(u) rootEqn(u, rnd(ii, 2), k(tauU(ii)), g(tauL(ii)), k(tauL(ii)), g(tauU(ii)), t(ii)); % SJC (symmetric)
    else
        % Similarly for JC copula
        rootEqn2 = @(u) rootEqn(u, rnd(ii, 2), k(tauU(ii)), g(tauL(ii)), t(ii));                           % JC (non-symmetric)
    end
    [x, fval] = bisection(rootEqn2, eps, 1 - eps); 
    
    %rnd(ii, 1) = x; % works for standard loop
    rndTemp(ii) = x; % need simper indexing for parallel loop
end
rnd(:, 1) = rndTemp;

% Reset seed to default setting
s = RandStream('mt19937ar', 'Seed', 0);
RandStream.setGlobalStream(s);
