function [T, extremalIdx] = armaPerformanceMap(modelData, ...
    maxlagAR, maxlagMA, metric, plot, dataName)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Function collects ARMA model performance according to some 'metric' 
    % and maps this into digestible table format where rows are MA lags and
    % columns are AR lags.
    % Args:
    %   - modelData = N * 1 struct of N fitted ARMA models (nb: fitted arma
    %   models follow a user defined data structure 
    %   - maxlagAR = scalar, maximum number of lags for AR polynomial
    %   - maxlagMA = scalar, maximum number of lags for MA polynomial
    %   - metric = string {'logL', 'aic', 'bic'} for desired performance 
    %   metric 
    %   - plot = {true, false} for plotting heat map
    %   - dataName = string w/ data name for plotting output
    % Output
    %   - T = table of performance metrics; x-axis = AR order; y-axis = MA
    %   order
    %   - extremalIdx = scalar or vector containing position in modelData 
    %   of model that has the best performance according to 'metric'
    % Nb:
    %   - 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Check inputs
    if size(modelData, 1) > 1 && size(modelData, 2) > 1
        error('input data should be a vector')
    end
    
    % Select relevant data according to desired performacne metric
    if strcmp(metric, 'logL') == 1
        dataPerf = vertcat(modelData.logL);
    elseif strcmp(metric, 'aic') == 1
        dataPerf = vertcat(modelData.aic);
    elseif strcmp(metric, 'bic') == 1
        dataPerf = vertcat(modelData.bic);
    else
        disp('error: check performance metric')
    end
          
    % Reverse mapping (here data should be an (maxlagMA + 1) * (maxlagAR +
    % 1) double
    mapPerf = @(data) reshape(data, maxlagMA + 1, maxlagAR + 1);
     
    % Performance mappings for log likelihood, AIC and BIC
    varNames = cell(1, maxlagAR + 1);
    for i = 1:maxlagAR + 1
        varNames{i} = strcat('AR', num2str(i - 1));   
    end
    rowNames = cell(1, maxlagAR + 1);
    for i = 1:maxlagAR + 1
        rowNames{i} =  strcat('MA', num2str(i - 1));   
    end
    
    % Reshape performance data
    A = mapPerf(dataPerf);
%     % Reshape test
%     M = [00;01;02;03;04;05;10;11;12;13;14;15;20;21;22;23;24;25]; % model data is stored like this for ARMA(2,5)
%     reshape(M, 5 + 1, 2 + 1);
       
    % Convert to table output
    sprintf('%s%s%s' , 'Table based on criteria: "', metric, '" generated.')
    T = array2table(A, 'VariableNames', varNames, 'RowNames', rowNames);
     
    % Visualise
    if plot == true
        figure;
        heatmap(varNames, rowNames, A, 'Title', sprintf('%s (%s)', dataName, metric));
    else
    end
    
    % Output best model according to performance metric
    if strcmp(metric, 'logL') == 1
        extremalIdx = find(A == max(A(:)));    % maximum for log likelihood
    elseif strcmp(metric, 'aic') == 1 || strcmp(metric, 'bic')
        extremalIdx = find(A == min(A(:)));    % minimum for AIC / BIC
    else
        disp('error: check performance metric')
    end
    [rowIdx, colIdx] = ind2sub([(maxlagMA + 1), (maxlagAR + 1)], ...
        extremalIdx);                       % indices for extremal value
    remapModelData = mapPerf(modelData);    % reshape data
    for i = 1:length(rowIdx)                % output respective model info
        remapModelData(rowIdx(i), colIdx(i)).EstMdl
    end
    
end