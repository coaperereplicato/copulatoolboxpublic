function h = exceedanceCorrPlot(T, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function plots exceedance correlations based on output from
% exceedanceCorr() call
% Args:
%   - T (required) = table output from exceedanceCorr()
%   - 'method' (optional, string); either 'Pearson' (default), 'Spearman', 
%   or 'Kendall'
%   - 'varNames' (optional, 2 * 1 cell array of strings)
%   - 'yaxis' (optional, 2 * 1 matrix of variables in unit interval), ex.
%   [0,1] or [0.5, 0.75]
%   - 'subplot' (optional, logical); true creates new figure, false 
%   (default) does not; same as 'holdplot' but title is changed [fix duplication]
%   - 'holdplot' (optional, logical); true creates new figure, false
%   (default) does not; useful for overlaying plot in this function over
%   another
% Output
%   - N/A
% Nb:
%   - for a definiton of exceedance correlation see ex. Patton 
%   (2004, p. 138)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create InputParser object (http://au.mathworks.com/help/matlab/matlab_prog/parse-function-inputs.html)
p = inputParser;

defaultMethod = 'Pearson';
validMethods ={'Pearson', 'Spearman', 'Kendall'};
checkMethods = @(x) any(validatestring(x, validMethods));

defaultYaxis = 'auto';

defaultVarNames = '';

defaultSubplot = false;

defaultHoldPlot = false; % prevent new figure generation (useful to overlay plots)

% Add inputs to scheme
addRequired(p, 'T');
addOptional(p, 'method', defaultMethod, checkMethods)
addOptional(p, 'subplot', defaultSubplot, @islogical)
addOptional(p, 'holdplot', defaultHoldPlot, @islogical)
addOptional(p, 'yaxis', defaultYaxis)
addOptional(p, 'varNames', defaultVarNames)


% Parse (requiring exact matches)
p.KeepUnmatched = false;
parse(p, T, varargin{:})

% Get optional input
corrType = p.Results.method;
varNames = p.Results.varNames;
yaxis = p.Results.yaxis;
subplot = p.Results.subplot;
holdplot = p.Results.holdplot;

% Select correlation to use 
corrTemp = strcat('corr', corrType);
temp = strcmp(T.Properties.VariableNames, corrTemp); % mask for location of non parametric correlation measure
if strcmp(corrType, 'Pearson') 
    % parametric correlation chosen 
    idxCorr = find(strcmp(T.Properties.VariableNames, 'corr'));  
elseif sum(temp) == 0 
    % non-parametric data not available
    error('non-parametric correlation data not available')
else
    % non-parametric correlation available & used
    idxCorr = find(temp);
end

% Variable names
myTitle = sprintf('Exceedance (%s) Correlations', corrType);
if ~isempty(varNames)
   myTitle = strcat(myTitle, sprintf(' (%s & %s)', varNames{1}, varNames{2}));
end

% Plot
if subplot
    % do not create new figure in this case & adjust titles for subplots
    myTitle =  sprintf('%s & %s', varNames{1}, varNames{2}); 
elseif holdplot
    % do not create new figure in this case
else
    figure;                                                  %  only create new figure when fn used for subplotting 
end
%scatter(T.pctile, T.(idxCorr))
h = plot(T.pctile, T.(idxCorr), '-');
title(myTitle)
xlabel('percentile')
ylabel('correlation')

% Set y limits if specified
if strcmp(yaxis, 'auto') ~= 1
    ylim(yaxis)
end

