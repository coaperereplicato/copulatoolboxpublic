function rnd = copula2D_rnd(copula, copulaParams, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function draws sample from specified bivariate copula
% Args:
%   - copula = string containing copula name (see validCopulas variable 
%   below for options)
%   - copulaParams = cell array containing names and corresponding values
%   of copula parameters; ex. {'tauU', [0.25, 0.20], 'tauL', [0.30, 0.25]};
%   note that the function can handle vectors of parameters. For copulas
%   built into matlab, all but the student-t takes only one parameter.
%   Thus, any parameter name can be given for copulas other than student-t,
%   for student-t the parameter names are: 'rho' & 'nu'.
%   - 'numSim' = (optional) integer; number of draws; defaults to
%   length(tauX) if tauX is a vector (here X = U,L)
%   - 'seed' = (optional) nonnegative scalar integer with which to 
%   initialize all streams (this fn default = 'shuffle' which creates seed
%   based on the current time)
%   - 'useParallel' = (optional) logical, false (default) => serial 
%   (nonsequential) processing; true => parallel processing. Nb: (i) set 
%   default to false to avoid initialising parallel pool for low
%   dimensional simulation calls; (ii) max no. workers set to 12 in code.
% Output:
%   - rnd = 2 * N (or 2 * numSim) matrix containng sample [u, v] 
% Nb:
%   - Function is not properly tested (ex. against Patton's SJC rnd)
%   - If copula parameters are scalars and numSim > 1, then every 
%   simulation uses the same (unique) parameters. If the parameters are 
%   vectors then then numSim reverts to length(parameter_k) =
%   length(parameter_1) and each simulation uses a different element in the
%   numSim * 2 matrix, [parameter_1, parameter_2, ..., parameter_k].
%   - function designed for bivariate copula only, so if parameter nubmer 
%   k is a vector of length N, then the function treats this as the user 
%   wanting N simulations based on the N different values specified for
%   the kth parameter.
%   - Pseudo code:
%       1. draw v ~ U(0,1) w/ rand()
%       2. draw t ~ U(0,1) w/ rand()
%       3. solve for u where t = C_v(u)
%       4. return (u,v)
% Ex.
%   - copulaParams = {'tauU', [0.25, 0.30], 'tauL', [0.30, 0.20]};
%   - copula2D_rnd('SymmetricJoeClayton', {'tauU', [0.25, 0.30], 'tauL', [0.30, 0.20]})
%   - copula2D_rnd('JoeClayton', {'tauU', 0.25, 'tauL', 0.30}, 'numSim', 10)
%   - copula2D_rnd('SymmetricJoeClayton', {'tauU', [0.25, 0.30], 'tauL', [0.30, 0.20]}, 'numSim', 10)
%   - copula2D_rnd('t', {'rho', 0.4, 'nu', 6}, 'numSim', 3, 'useParallel', false)
%   - copula2D_rnd('t', {'rho', [0.4, 0.6], 'nu', [6, 10]}, 'useParallel', false)
%   - copula2D_rnd('Gaussian', {'rho', linspace(0.1, 0.9, 10)})
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create input parser object 
p = inputParser;

% Default number of draws
defaultNumSim = 1;
checkNumSim = @(x) ~mod(x, 1);   % check if integer

% Default seed
defaultSeed = 'shuffle';         % used to set global stream according current time

% Check copula input
validCopulas ={'Gaussian', 't', 'Clayton', 'Frank', 'Gumbel', 'RotatedGumbel', 'JoeClayton', 'SymmetricJoeClayton'};
checkCopulas = @(x) any(validatestring(x, validCopulas));

% Default setting for parallel processing
defaultParallel = false;     % false for serial (non-sequential) processing, true for parallel; default to false to avoid initialising parallel pool for single rv draws

% Add inputs to scheme
addRequired(p, 'copula', checkCopulas);
addRequired(p, 'copulaParams', @iscell);
addOptional(p, 'numSim', defaultNumSim, checkNumSim)
addOptional(p, 'seed', defaultSeed)
addOptional(p, 'useParallel', defaultParallel, @islogical)

% Parse (requiring exact matches)
p.KeepUnmatched = false;
parse(p, copula, copulaParams, varargin{:})

% Get optional inputs
numSim = p.Results.numSim;
seed = p.Results.seed;
useParallel = p.Results.useParallel;

% Set seed (global random stream according to user specified seed or default current time); ref.: https://au.mathworks.com/help/matlab/ref/randstream.randstream.html
s = RandStream('mt19937ar', 'Seed', seed); 
RandStream.setGlobalStream(s);

% Get copula parameters (using my own name-value parser)
params = nameValuePairParser(copulaParams);  
numParams = size(params, 2);

% Check copula parameters have same dimensions
% Nb: function for bivariate copula only so if parameter nubmer M is a
% vector of length N, then the function treats this as the user wanting N
% simulations based on the N different values specified for parameter M
for ii = 1:1:(numParams - 1)
    if sum(size(params.(1)) == size(params.(ii + 1))) ~= 2
        error('parameters may be vectors, but must all have the same length (and orientation)')
    end
end

% Flag if parameters are passed as vectors & adjust number of simulations
if isscalar(params.(1))            % first parameter suffies as last if statement ensures all parameters have same dimens
    parameterVectors = false;
else
    parameterVectors = true;
    numSim = length(params.(1));
    warning('numSim adjusted')
end

% Copula specific simulation
switch copula
    
    % Matlab built-in copulas
    case {'Gaussian', 't', 'Clayton', 'Frank', 'Gumbel'}   
        
        % Case dependent simulation
        if parameterVectors
            % Loop through parameters if parameters are vectors           
            rnd = - 999.99 * ones(numSim, 2);
            switch copula
                case 't'
                    for ii = 1:1:numSim
                        rnd(ii, :) = copularnd('t', params.rho(ii), params.nu(ii), 1); % bivariate student-t takes two parameters
                    end   
                otherwise
                    for ii = 1:1:numSim
                        rnd(ii, :) = copularnd(copula, params.(1)(ii), 1);             % {'Gaussian', 'Clayton', 'Frank', 'Gumbel'} are all one parameter copulas for bivariate case
                    end
            end  
        else
            % Parameters are scalar
            switch copula
                case 't'
                    rnd = copularnd('t', params.rho, params.nu, numSim);
                otherwise
                    rnd = copularnd(copula, params.(1), numSim);
            end
        end
         
    % Developer created copulas
    case {'RotatedGumbel', 'JoeClayton', 'SymmetricJoeClayton'}
      
        % Stretch parameters if scalar and numSim > 1 (awkward b/c table formatting)
        if ~parameterVectors && numSim > 1
            temp = NaN(numSim, numParams); 
            for ii = 1:1:numParams
                temp(:, ii) = params.(ii) .* ones(numSim, 1);
            end
            params = array2table(temp, 'VariableNames', params.Properties.VariableNames);
        end

        % Draw from V ~ U(0,1) and t ~ U(0,1), where V & t are independent
        rnd = - 999.99 * ones(numSim, 2); 
        rnd(:, 2) = rand(numSim, 1);      
        t = rand(numSim, 1);      
        
        % Get root equation for conditional copula
        switch copula
            case 'JoeClayton'
                rootEqn = @(u, v, k, g, t) copulaJC_cdfUgivenV(u, v, k, g) - t;
            case 'SymmetricJoeClayton'
                condCopula = @(u, v, k1, g1, k2, g2) 0.5 * (copulaJC_cdfUgivenV(u, v, k1, g1) -  copulaJC_cdfUgivenV(1 - u, 1 - v, k2, g2) + 1); % cf. /LandEconomy/Mathematica/JC_copula.nb & Patton (2004, p. 164)
                rootEqn = @(u, v, k1, g1, k2, g2, t) condCopula(u, v, k1, g1, k2, g2) - t;
            case 'RotatedGumbel'
                disp('not yet implemented')
                rnd = NaN;
                return
        end
                
        % Get parameter transform functions for (S)JC copulas
        if strcmp(copula, 'JoeClayton') || strcmp(copula, 'SymmetricJoeClayton')
            k = @(tau) 1 ./ log2(2 - tau);
            g = @(tau) - 1 ./ log2(tau); 
        end
        
        % Set workers to emulate serial or parallel processing
        if useParallel
            maxNumWorkers = 12;    % parallel; 12 = default max number of workers
        else
            maxNumWorkers = 0;     % serial; parfor will execute serially but order need not be sequential (as would be the case w/ for loop)
        end
        
        % Simulate  u (given v)
        rndTemp = rnd(:, 1); % temporary variable needed to avoid issues in parfor loop
        tauUTemp = params.tauU; % accessing vector structure faster than table structure in loop below
        tauLtemp = params.tauL; % "                                                                  "
        parfor (ii = 1:1:numSim, maxNumWorkers)
            % Solve 't = C_v(u)' for 'u', where C_v(u) := cdfUgivenV(u, v, ., .) and t ~ U(0,1)
            switch copula
                case 'JoeClayton'
                    rootEqn2 = @(u) rootEqn(u, rnd(ii, 2), k(tauUTemp(ii)), g(tauLtemp(ii)), t(ii));
                case 'SymmetricJoeClayton'
                    rootEqn2 = @(u) rootEqn(u, rnd(ii, 2), k(tauUTemp(ii)), g(tauLtemp(ii)), k(tauLtemp(ii)), g(tauUTemp(ii)), t(ii));
                case 'RotatedGumbel'
                    disp('not implemented yet')
                    rootEqn2 = @(u) u .* u + 1; % rootless equation to raise warnings/errors in bisection call
            end
            [x, fval] = bisection(rootEqn2, eps, 1 - eps); 
            rndTemp(ii) = x;                    % need simpler indexing for parallel loop           
        end
        rnd(:, 1) = rndTemp;
end

% Reset seed to default setting
s = RandStream('mt19937ar', 'Seed', 0);
RandStream.setGlobalStream(s);
