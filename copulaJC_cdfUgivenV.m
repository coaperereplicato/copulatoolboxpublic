function out = copulaJC_cdfUgivenV(u, v, k, g)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function returns the distribution function of U evaluated at U = u,
% conditional on V = v, where (U, V) are described by the Joe-Clayton 
% copula. 
% Args:
%   - u = scalar or N * 1 vector of values in (0, 1)
%   - v = scalar or N * 1 vector of values in (0, 1)
%   - k = scalar transformation of upper tail parameter tauU
%   - g = scalar transformation of lower tail parameter tauL
% Outputs:
%   - scalar or N * 1 vector of values of cdf for U evaluated at U = u 
%   conditional on V = v, i.e. Pr(U <= u | V = v).
% Nb:
%   - Used in both Symmetric and regular Joe-Clayton simulations.
%   - Conditional density, Pr(U <= u | V = v), is equivalent to the partial 
%   derivative of the Joe-Clayton copula w.r.t. 'v'; see ex. Nelsen (2006),
%   An introduction to copulas, p. 41. 
%   - Algebra comes from /LandEconomy/Mathematica/JC_copula.nb
%   - From Patton (2004) Journal of Financial Econometrics, Vol. 2, No. 1
%   (p. 164):
%       > k(tau) = 1 ./ log2(2 - tau)
%       > g(tau) = - 1 ./ log2(tau),
%   where tau is not specified as upper or lower as we reverse arguments in
%   Symmetric-JC function.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
out = (1 - v).^(k - 1) ...
    .* (1 - (1 - v).^k).^(- g - 1) ...
    .* (1 - ((1 - (1 - u).^k).^(- g) + (1 - (1 - v).^k).^(- g) - 1).^(- 1 ./ g)).^(1 ./ k - 1) ...
    .* ((1 - (1 - u).^k).^(- g) + (1 - (1 - v).^k).^(- g) - 1).^(- 1 ./ g - 1);