function rnd = copulaBuitltin2D_rnd(copulaFit, copulaNames, numSim)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function wrapper simulates bivariate copulas using matlab's copularnd 
% function.
% fit.
% Args:
%   - copulaFit = cell array of fitted copula parameters
%   - copulaNames = cell array of characters denoting respective copulae; 
%   any combination of copulas that matlab's copularnd can handle are 
%   available, i.e. any combination of
%   {'Gaussian', 't', 'Clayton', 'Frank', 'Gumbel'}
%   - number of draws to generate
% Output
%   - rnd = cell array bivariate simulations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rnd = cell(1, length(copulaNames));
for ii = 1:1:length(copulaNames)
    if strcmp(copulaNames{ii}, 't')
        rnd{ii} = copularnd('t', copulaFit{ii}{1}, copulaFit{ii}{2}, numSim);  
    else
        rnd{ii} = copularnd(copulaNames{ii}, copulaFit{ii}, numSim);
    end
end