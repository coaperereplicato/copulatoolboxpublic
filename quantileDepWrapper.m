function [q, qDep, varargout] = quantileDepWrapper(u, v, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function wraps quantileDep.m and quantileDepDiff.m (incl. default 
% settings) 
% Args:
%   - u = (required) N * 1 vector of scalar data points 
%   - v = (required) N * 1 vector of scalar data points 
%   - 'qntleLower' = (optional) scalar; lower quantile starting point
%   - 'nPts' = (optional) scalar integer; number of quantiles to evaluate
%   function at
%   - 'qntleDiff' = (optional) logical; compute and output difference in 
%   upper and lower quantiles
%   - 'CIbounds' = (optional) logical; compute and output confidence bounds
%   associated w/ statistics via bootstrapping
% Output:
%   - q = vector of quantiles
%   - qDep = vector of value of quantile dependence function at q
%   - varargout{1} = vector of quantiles for difference statistic
%   - varargout{2} = vector of value of difference statistic at varargout{1}
%   - varargout{3} = vector of confidence intervals for quantile dependence 
%   statistics
%   - varargout{4} = vector of confidence intervals for difference in 
%   quantile dependence statistics
% Nb:
%   - See quantileDep() and quantileDepDiff() for further documentation
%   - Bootstrap for CI for quantile dependence statistics
%       > Issue w/ reliance on iid for bootstrap approach (see intro to wiki
%   article). Also, relates to CI for copula fit (see Patton (2012,  s.2.1)
%   and references therein).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create InputParser object (http://au.mathworks.com/help/matlab/matlab_prog/parse-function-inputs.html)
p = inputParser;

defaultGap = 0.025;
checkGap = @(x) x >= eps && x <= 0.5 - eps;

defaultNumPts = 150;
checkNumPts = @(x) ~mod(x, 1);   % check if integer

defaultQntleDiff = false; 

defaultCIbounds = false;         % confidence intervals associated w/ statistic
defaultBoot_numSim = 150;        % number of bootstrap samples to generate CI statistic
defaultBoot_CIalpha = 1 - 0.90;  % alpha for 90 pct confidence interval
defaultBoot_parallel = false;    % no parallel computing for bootstrap sampling

% Add inputs to scheme
addRequired(p, 'u', @isnumeric);
addRequired(p, 'v', @isnumeric);
addOptional(p, 'qntleLower', defaultGap, checkGap)
addOptional(p, 'nPts', defaultNumPts, checkNumPts)
addOptional(p, 'qntleDiff', defaultQntleDiff, @islogical)
addOptional(p, 'CIbounds', defaultCIbounds, @islogical)
addOptional(p, 'nSim', defaultBoot_numSim, checkNumPts)
addOptional(p, 'CIalpha', defaultBoot_CIalpha, @isnumeric)
addOptional(p, 'bootParallel', defaultBoot_parallel, @islogical)

% Parse (requiring exact matches)
p.KeepUnmatched = false;
parse(p, u, v, varargin{:})

% Get optional input (quantile fns)
nPts = p.Results.nPts;
qntleLower = p.Results.qntleLower;
qntleDiff = p.Results.qntleDiff;

% Get optional input (CI bootstrapping)
CIbounds = p.Results.CIbounds;
nSim = p.Results.nSim;
CIalpha = p.Results.CIalpha;
bootParallel = p.Results.bootParallel;

% Quantiles for function evaluation 
q = linspace(qntleLower, 1 - qntleLower, nPts);

% Quantile depednence (cf. Patton (2012, s 2.1.2) 
qDep = quantileDep(u, v, q);

% Difference in quantile dependence (cf. Patton (2012, s 2.1.2)
if qntleDiff
    %[qDepDiff, qDiff] = quantileDepDiff(q, qDep);
    [varargout{1}, varargout{2}] = quantileDepDiff(q, qDep);
end

% Compute confidience intervals by bootstrap simulation
if CIbounds
    % Set parallel vs serial computing option
    optionsBoot = statset('UseParallel', bootParallel);
    
    % Anonymous functions (of u & v) for bootstrp call
    qDepBoot  = @(u, v) quantileDep(u, v, q);                            % quantile dependence  
    if qntleDiff
        qDepBoot2 = @(u, v) quantileDepDiff(q, quantileDep(u, v, q));    % quantile dependene difference
    end
    
    % Bootstrap sample generation
    B  = bootstrp(nSim, qDepBoot,  u, v, 'Options', optionsBoot);      % bootstrap samples (rows), quantiles (columns)
    if qntleDiff
        B2 = bootstrp(nSim, qDepBoot2, u, v, 'Options', optionsBoot);  % bootstrap samples (rows), quantiles (columns)
    end
    
    % Preallocate for confidence intervals
    qDepCI = NaN(length(q), 2);
    if qntleDiff
        qDepDiffCI = NaN(length(q) / 2, 2);
    end
    
    % Compute confidence intervals based on percentile
    tempCI = @(M, idx, upper) quantile(M(:, idx), (1 - CIalpha / 2) * upper + CIalpha / 2 * (1 - upper));
    for ii = 1:length(q)
        qDepCI(ii, 1) = tempCI(B, ii, false);
        qDepCI(ii, 2) = tempCI(B, ii, true);
        if qntleDiff && ii <= length(q) / 2  % difference statistic applies for half the quantiles
            qDepDiffCI(ii, 1) = tempCI(B2, ii, false);
            qDepDiffCI(ii, 2) = tempCI(B2, ii, true);
        end
    end
    
    % Return confidence intervals
    varargout{3} = qDepCI;
    if qntleDiff
        varargout{4} = qDepDiffCI;
    end
end


