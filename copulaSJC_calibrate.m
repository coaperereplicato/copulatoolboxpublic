function [thetaMLE, fval] = copulaSJC_calibrate(u, v, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function calibrates (Symmetric) Joe-Clayton copula to bivariate data set
% w/ marginals u and v.
% Args:
%   - u = N * 1 vector for marginal distribution 1
%   - v = N * 1 vector for marginal distribution 2
%   - 'theta0' = (optional) vector of parameter, theta = [tauU; tauL], 
%   starting point
%   - 'lb' = (optional) vector of lower bounds on theta
%   - 'ub' = (optional) vector of upper bounds on theta
%   - 'copula' = (optional) 'JoeClayton' or 'SymmetricJoeClayton' (default)
%   - 'options' = (optional) stucture containing fmincon() options
% Outputs:
%   - thetaMLE = MLE estimate of theta = [tauU; tauL]
%   - fval = negative of copula likelihood at optimal parameter set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create input parser object 
p = inputParser;

% Default starting point for parameters theta = [tauU; tauL]
defaultStart = [0.5; 0.5];

% Default bounds on theta = [tauU; tauL]
defaultUB = [1, 1];
defaultLB = [0, 0];

% Default copula
defaultCopula = 'SymmetricJoeClayton'; 
validCopulas ={'JoeClayton', 'SymmetricJoeClayton'};
checkCopulas = @(x) any(validatestring(x, validCopulas));

% Default fmincon options
defaultOptions = optimset('Display', 'iter', 'TolCon', 1e-12, 'TolFun', 1e-4, 'TolX', 1e-6);
%optimset('Display', 'iter', 'TolCon', 1e-12, 'TolFun', 1e-4, 'TolX', 1e-6);


% Add inputs to scheme
addRequired(p, 'u', @isnumeric);
addRequired(p, 'v', @isnumeric);
addOptional(p, 'theta0', defaultStart, @isnumeric)
addOptional(p, 'lb', defaultLB, @isnumeric)
addOptional(p, 'ub', defaultUB, @isnumeric)
addOptional(p, 'copula', defaultCopula, checkCopulas)
addOptional(p, 'options', defaultOptions, @isstruct)

% Parse (requiring exact matches)
p.KeepUnmatched = false;
parse(p, u, v, varargin{:})

% Get optional inputs
theta0 = p.Results.theta0;
lb = p.Results.lb;
ub = p.Results.ub;
copula = p.Results.copula;
options = p.Results.options;

% Get copula type
if strcmp(copula, 'JoeClayton')
    % Regular JC
    symmetric = false;
else
    % Symmetric JC
    symmetric = true;
end

% Set objective fn
% Nb:
%   - negative of logL for fmincon() call
%   - params = [tauU; tauL] (as per copulaSJC_logL.m signature)
objFn = @(params) - 1 * copulaSJC_logL(u, v, params(1), params(2), symmetric);

% Calibrate
[thetaMLE, fval] = fmincon(objFn, theta0, [], [], [], [], lb, ub, [], options);

% Transpose result if required (function always returns column vector,
% irrespective of orientation of user defined theta0)
if size(thetaMLE, 2) ~= 1
    thetaMLE = thetaMLE';
end