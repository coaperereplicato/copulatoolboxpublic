function [copulaFit, copulaNames] = copulaBuitltin2D_calibrate(u, v, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function wrapper calibrates copula to bivariate data set using matlab's 
% copulafit.
% Args:
%   - u (required) = N * 1 vector of data from margin 1
%   - v (required) = N * 1 vector of data from margin 2
%   - 'copula' (optional, cell array of characters); any combination of
%   copulas that matlab's copula fit can handle, i.e. any combination of 
%   {'Gaussian', 't', 'Clayton', 'Frank', 'Gumbel'}
% Output
%   - copulaFit = cell array of fitted copula parameters
%   - copulaNames = cell array of characters denoting respective copulae
%   used
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create InputParser object (http://au.mathworks.com/help/matlab/matlab_prog/parse-function-inputs.html)
p = inputParser;

% Default for copula choice
builtinCopulas = {'Gaussian', 't', 'Clayton', 'Frank', 'Gumbel'};

% Add inputs to scheme
addRequired(p, 'u', @isvector);
addRequired(p, 'v', @isvector);
addOptional(p, 'copula', builtinCopulas)

% Parse (requiring exact matches)
p.KeepUnmatched = false;
parse(p, u, v, varargin{:})

% Get optional input
copulaNames = p.Results.copula;

% Calibrate (matlab builtin) copulas to std residual probability transform data
copulaFit = cell(1, length(copulaNames));
for ii = 1:1:length(copulaNames)
    if strcmp(copulaNames{ii}, 'Gaussian')
        rhohat = copulafit('Gaussian', [u, v]);
        copulaFit{ii} = rhohat(2);                             % correlation 
    elseif strcmp(copulaNames{ii}, 't')
        [rhohat, nuhat] = copulafit('t', [u, v]);
        copulaFit{ii} = {rhohat(2), nuhat};                    % correlation & dof
    else
        copulaFit{ii} = copulafit(copulaNames{ii}, [u, v]); % 1 parameter (archimedean family)
    end
end