function copulaPlot3D(copula, copulaParams, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function plots bivariate copula {pdf, cdf} or corresponding contour plot. 
% Args:
%   - copula = string containing copula name
%   - copulaParams = cell array containing names and corresponding values
%   of copula parameters; ex. {'tauU', 0.25, 'tauL', 0.30}
%   - 'plotType' = (optional) string containing plot type, 'pdf' or 'cdf'
%   - 'showAnnotation' = (optional) logical specifying if copula parameters
%   should be shown on plot
%   - 'annotationLocation' = (optional) four-element vector of the form 
%   [x, y, w, h] specifying location of annotation box; the x and y 
%   elements determine the position and the w and h elements determine the 
%   size.
%   - 'zLimUpper' = (optional) string 'auto' or scalar specifying the
%   height of the 3D plot; (only useful for pdf, cdf & contours do not
%   require)
%   - 'contourPlot' = (optional) logical; plot contour of pdf/cdf instead 
%   of pdf/cdf itself
%   - 'contourLevels' = (optional) vector of heights at which contours are
%   plotted; (useful for increasing number of contours for cdf contour
%   plot)
%   - 'uDom' = (optional) vector on (0,1) containing points at which plot
%   will be evaluated on margin 1
%   - 'vDom' = (optional) vector on (0,1) containing points at which plot
%   will be evaluated on margin 2
% Outputs:
%   - N/A
% Nb:
%   - annotationLocation = [0.15, 0.6, 0.3, 0.3] works for contour plots; 
%   [0.2, 0.5, 0.3, 0.3] works for pdf/cdf if plot is properly rotated
% Ex. of call
% copulaPlot3D('SymmetricJoeClayton', {'tauU', 0.25, 'tauL', 0.30}, ...
%   'plotType', 'cdf', 'contourPlot', true, ...
%   'contourLevels', linspace(sqrt(eps), 1 - sqrt(eps), 10))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create input parser object 
p = inputParser;

% Check if number is natural
isnatural = @(x) isscalar(x) && (mod(x, 1) == 0) && x > 0;   % natural number

% Default domains for meshgrid
numDom = 40;
startDom =  eps; % too small can result in pdf = Inf; esp can be good b/c extreme values go to Inf so that they are not plotted
defaultuDom = linspace(startDom, 1 - eps, numDom)';
defaultvDom = linspace(startDom, 1 - eps, numDom)';

% Default plot type
defaultPlot = 'pdf';
validPlots = {'cdf', 'pdf'};
checkPlots = @(x) any(validatestring(x, validPlots));

% Default zlimits for plot
defaultZlimUpper = 'auto';

% Default annotation setting
defaultAnnotation = true;

% Default location of annotation textbox in plot
deafaultLocation = [0.2, 0.5, 0.3, 0.3]; % [0.15, 0.6, 0.3, 0.3] works for contour plots

% Default contours
defaultContour = false;

% Default number of contour lines
defaultNumContours = 15;

% Default heights for countour plots
defaultLevels = NaN; % we will set default below based on data
%defaultLevels = linspace(0.01, 5, 30); % for cdf linspace(sqrt(eps), 1 - sqrt(eps), N) gives N contours (2 of which are barely visible in general)

% Valid copulas
validCopulas ={'JoeClayton', 'SymmetricJoeClayton'};
checkCopulas = @(x) any(validatestring(x, validCopulas));

% Add inputs to scheme
addRequired(p, 'copula', checkCopulas);
addRequired(p, 'copulaParams', @iscell);
addOptional(p, 'plotType', defaultPlot, checkPlots)
addOptional(p, 'showAnnotation', defaultAnnotation, @islogical)
addOptional(p, 'annotationLocation', deafaultLocation, @isnumeric)
addOptional(p, 'zLimUpper', defaultZlimUpper)
addOptional(p, 'contourPlot', defaultContour, @islogical)
addOptional(p, 'contourLevels', defaultLevels, @isnumeric)
addOptional(p, 'numContours', defaultNumContours, isnatural)
addOptional(p, 'uDom', defaultuDom, @isnumeric)
addOptional(p, 'vDom', defaultvDom, @isnumeric)

% Parse (requiring exact matches)
p.KeepUnmatched = false;
parse(p, copula, copulaParams, varargin{:})

% Get optional inputs
plotType = p.Results.plotType;
showAnnotation = p.Results.showAnnotation;
annotationLocation = p.Results.annotationLocation;
zLimUpper = p.Results.zLimUpper;
contourLevels = p.Results.contourLevels;
contourPlot = p.Results.contourPlot;
numContours = p.Results.numContours;
uDom = p.Results.uDom;
vDom = p.Results.vDom;

% Initialise grid & output function values
[gridTempU, gridTempV] = meshgrid(uDom, vDom);
plotTemp = -ones(size(gridTempU));

% Set range plotting limits for pdf
if strcmp(plotType, 'pdf')
    if isnumeric(zLimUpper)
        zLimits = [0, zLimUpper];   % need to cap z limit as pdf can be peaked for extreme values of margins
    else
        zLimits = zLimUpper;        % default is auto limits 
    end
else
    zLimits = 'auto';               % not an issue for cdf
end

% Get data for plotting
if strcmp(copula, 'JoeClayton') || strcmp(copula, 'SymmetricJoeClayton')
    % Set JC or SJC
    if strcmp(copula, 'JoeClayton')
        symmetric = false;
    else
        symmetric = true;
    end
    
    % Get copula parameters (using my own name-value parser)
    params = nameValuePairParser(copulaParams);  
   
    % Set pdf or cdf copula function
    if strcmp(plotType, 'pdf')
        plotFn = @(u, v, tauU, tauL) copulaSJC_pdf(u, v, tauU, tauL, symmetric);  
    else
        plotFn = @(u, v, tauU, tauL) copulaSJC_cdf(u, v, tauU, tauL, symmetric);    
    end
    
    % Compute z values for relevant plot
    for jj = 1:1:size(gridTempU, 2)
        plotTemp(:, jj) = plotFn(gridTempU(:, jj), gridTempV(:, jj), params.tauU, params.tauL);
    end
   
    % Other copula cases to be implemented here
    % ...
    % ...
end

% Set string for parameter info output on plot
if showAnnotation
    str = '';
    for ii = 1:1:size(params, 2)
        str = strcat(str, params.Properties.VariableNames{ii}, '=%.2f\n');  % new paramter on a new line
    end
    str = str(1:end - 2);                                                   % get rid of last unwanted \n (newline)
    str = sprintf(str, params{1, :});                                       % add parameter values
end

% Plot {pdf, cdf} or {pdf contour, cdf contour}
figure;
plotTitle = sprintf('%s %s', copula, plotType);
if contourPlot  
    % If contour levels specified use them, o/wise use (possibly user defined) natural number of contour levels  
    if isnan(contourLevels)
        contourParam = numContours;     % natural number
    else
        contourParam = contourLevels;   % linspace
    end
    
    % Contour plot (for pdf or cdf)
    contour(uDom, vDom, plotTemp, contourParam)
    title(plotTitle);
%     
%     % Contour plot (for pdf or cdf)
%     contour(uDom, vDom, plotTemp, contourLevels)
%     title(plotTitle);
else
    % Pdf or cdf
    surf(uDom, vDom, plotTemp)
    zlim(zLimits)
end

% Add {title, labels, annotation} to plot
xlabel('u')
ylabel('v')
title(plotTitle);
if showAnnotation
    a = annotation('textbox', annotationLocation, 'String', str, 'FitBoxToText', 'on');
    a.BackgroundColor = 'white';
end