function [qDepDiff, qDiff] = quantileDepDiff(q, qDep)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function computes difference in 'upper' and 'lower quantile dependence 
% functions, cf. quantileDep().
% Args:
%   - q = k * 1 vector of quantiles (used to generate qDep)
%   - qDep = N * 1 vector of quantile dependence statistics for each
%   quantile in q
% Output:
%   - qDepDiff = k / 2 * 1 vector of difference in 'upper' and 'lower'  
%   quantile dependence statistics evaluated at each quantile in qdiff
%   in q
%   - qDiff =  k / 2 * 1 vector of quantiles for difference in quantile
%   dependence statistics
% Nb:
%   - 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nqPts = length(q);
if mod(nqPts, 2) ~= 0
    error('set number of quantiles to an even number for difference statistic')
end
qDepDiff = flip(qDep(nqPts / 2 + 1:end)) - qDep(1:nqPts / 2);
qDiff = q(1:nqPts / 2);