function logL = copulaSJC_logL(u, v, tauU, tauL, symmetric)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function computes log likelihood for (Symmetric) Joe-Clayton copula.
% Inputs:
%   - u = N * 1 vector of data from marginal distribution 1
%   - v = N * 1 vector of data from marginal distribution 2
%   - tauU = scalar 
%   - tauL = scalar
%   - symmetric = (optional) logical for symmetric (default) vs regular JC 
% Output:
%   - logL = scalar, log likelihoood
% Details:
%   - see copulaSJC_pdf.m documentation for more function {input, output,
%   notes} information
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Log likelihood
logL2 = @(bool) sum(log(copulaSJC_pdf(u, v, tauU, tauL, bool)));

% Output desired likelihood (regular JC iff symmetric = false)
if nargin == 5 && ~symmetric
    % Regular Joe-Clayton
    logL = logL2(false);
else
    % Symmetric Joe-Clayton
    logL = logL2(true);
end