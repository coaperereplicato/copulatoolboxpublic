function cdf = empiricalCDF(data)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Function computes empirical cdf based on vector of data.
    % Args:
    %   - N * 1 vector of scalar data points
    % Output
    %   - N * 1 vector of empirical probabiltiies (sorted acording to order
    %   of data)
    % Nb:
    %   - divide by (n + 1) to avoid empirical CDF having value of 1 at
    %   upper bound of support (o/wise issues with taking inverse cdf
    %   transform)
    %   - cf. https://en.wikipedia.org/wiki/Empirical_distribution_function
    %   and Patton (2012, eq. 3)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if size(data, 2) > 1
        error('data should be an n * 1 matrix')
    end
    
    n = length(data);
    temp = [data, (1:n)'];
    temp = sortrows(temp, 1);
    temp =  [temp, (1:n)' ./ (n + 1)]; % nb: n + 1
    temp = sortrows(temp, 2);
    cdf = temp(:, 3);
end