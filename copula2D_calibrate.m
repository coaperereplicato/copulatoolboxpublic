function [thetaMLE, fval] = copula2D_calibrate(copula, u, v, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function calibrates copula to bivariate dataset on (0, 1) x (0, 1).
% Matlab's built-in copula functionality is used along with some additional
% copulas that are optimised with relevant log likelihood function and
% fmincon.
% Args:
%   - copula = string containing copula name (see validCopulas variable 
%   below for options)
%   - u = N * 1 vector of data for marginal distribution 1
%   - v = N * 1 vector of data for marginal distribution 2
%   - 'theta0' = (optional) vector of initial guess for parameter vector
%   (only relevant for non built-in copulas)
%   - 'lb' = (optional) vector of lower bounds on parameter vector (only 
%   relevant for non built-in copulas)
%   - 'ub' = (optional) vector of upper bounds on parameter vector (only 
%   relevant for non built-in copulas)
% Output:
%   - thetaMLE = MLE estimate of copula parameter(s); for the Archemedian 
%   copulas this is a scalar, for (S)JC it is a vector of length 2.
%   - fval = negative of copula likelihood at optimal parameter set; not
%   available for matlab built-in copulas.
% Nb:
%   - For (S)JC, the parameter vector is specified as theta = [tauU; tauL]
%   this ordering is the same for (optional) specification of
%   {ub, lb, theta0}.
% Ex.
%   - temp = copularnd('t', 0.4, 6, 10000); u = temp(:, 1); v = temp(:, 2)              % simulate some data
%   - copula2D_calibrate('Gaussian', u, v)
%   - copula2D_calibrate('RotatedGumbel', u, v)
%   - copula2D_calibrate('t', u, v)
%   - copula2D_calibrate('t', u, v, 'lb', [0, 0], 'ub', [1, 1],'theta0', [0.5, 0.5])    % throws warning b/c redundant parameters
%   - copula2D_calibrate('SymmetricJoeClayton', u, v, 'theta0', [0.5, 0.5], 'lb', [0, 0], 'ub', [1, 1])
%   - copula2D_calibrate('SymmetricJoeClayton', u, v)                                   % use default initial parameter guess and bounds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create input parser object 
p = inputParser;

% Default starting point for parameters (copula specific defaults implemented below)
defaultStart = NaN; 

% Default bounds on parameters (copula specific defaults implemented below)
defaultUB = NaN; 
defaultLB = NaN; 

% Default copula
validCopulas ={'JoeClayton', 'SymmetricJoeClayton', 't', 'Gaussian', 'Clayton', 'Frank', 'Gumbel', 'RotatedGumbel'};
checkCopulas = @(x) any(validatestring(x, validCopulas));

% Default fmincon options
defaultOptions = optimset('TolCon', 1e-12, 'TolFun', 1e-4, 'TolX', 1e-6); % optimset('Display', 'iter', 'TolCon', 1e-12, 'TolFun', 1e-4, 'TolX', 1e-6);

% Add inputs to scheme
addRequired(p, 'copula', checkCopulas)
addRequired(p, 'u', @isnumeric);
addRequired(p, 'v', @isnumeric);
addOptional(p, 'theta0', defaultStart, @isnumeric)
addOptional(p, 'lb', defaultLB, @isnumeric)
addOptional(p, 'ub', defaultUB, @isnumeric)
addOptional(p, 'options', defaultOptions, @isstruct)

% Parse (requiring exact matches)
p.KeepUnmatched = false;
parse(p, copula, u, v, varargin{:})

% Get optional inputs
theta0 = p.Results.theta0;
lb = p.Results.lb;
ub = p.Results.ub;
options = p.Results.options;

% Case specific MLE
switch copula
    % Matlab built-in copulas
    case {'t', 'Gaussian', 'Clayton', 'Frank', 'Gumbel', 'RotatedGumbel'}
        
        % Notify user if redundant inputs have been passed (matlab's copulafit does not require initial parameter guess, parameter bounds nor fmincon options)
        testParam = @(redundantParam) ~any(strcmp(redundantParam, p.UsingDefaults));                   % test to see if redundant parameter was one passed (nb. redundantParam should be a string) 
        if testParam('theta0') || testParam('lb') || testParam('ub') || testParam('options')
            warning('ub, lb, theta0, and fmincon options are unused for the copula type passed.')
        end
        
        % MLE
        fval = NaN;     % no function value output for matlab's copulafit
        switch copula
            case 'Gaussian'
                rhohat = copulafit('Gaussian', [u, v]);
                thetaMLE = rhohat(2);                             % correlation 
            case 't'
                [rhohat, nuhat] = copulafit('t', [u, v]);
                thetaMLE = [rhohat(2), nuhat];                    % correlation & dof, respectively
            case {'Clayton', 'Frank', 'Gumbel'}
                thetaMLE = copulafit(copula, [u, v]);             % 1 parameter (archimedean family)
            case 'RotatedGumbel'
                thetaMLE = copulafit('Gumbel', [1 - u, 1 - v]);   % reverse input data to calibrate to rotated gumbel
        end
    
    % Developer created copulas    
    case {'JoeClayton', 'SymmetricJoeClayton'}  
        
        % Get symmetry flag
        if strcmp(copula, 'SymmetricJoeClayton'), symmetric = true; else, symmetric = false; end
        
        % Set objective fn
        % Nb:
        %   - negative of logL for fmincon() call
        %   - params = [tauU; tauL] (as per copulaSJC_logL.m function signature)
        objFn = @(params) - 1 * copulaSJC_logL(u, v, params(1), params(2), symmetric);
        
        % Default parameter bounds and initial guess (for tauU and tauL, respectively) unless specified
        if isnan(ub), ub = [1, 1]; end
        if isnan(lb), lb = [0, 0]; end
        if isnan(theta0), theta0 = [0.5, 0.5]; end
               
        % Calibrate
        [thetaMLE, fval] = fmincon(objFn, theta0, [], [], [], [], lb, ub, [], options);

end