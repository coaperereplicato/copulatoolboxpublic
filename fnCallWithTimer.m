function varargout = fnCallWithTimer(numArgOut, fn, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function wrapper that calls function passed and prints elapsed time
% Args:
%   - numArgOut = number of output variables; can specify as '' if the
%   funciton does not have a variable number of outputs depending on call
%   - fn = function handle
%   - varargin = arguments of fn
% Output
%    - varargout = output of fn
% Nb:
%    - function handle (not function) should be passed
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tStart = tic;
if isnumeric(numArgOut)
    % use specified number of variables out (case avoids issue w/ functions whose number of output variables depends on function call)
    [varargout{1:numArgOut}] = fn(varargin{:});
else
    % no need to specify number of output variables if they don't depend on function call
    [varargout{1:nargout(fn)}] = fn(varargin{:});
end
fprintf('%s call elapsed time: %.2f(s)\n', func2str(fn), toc(tStart))
