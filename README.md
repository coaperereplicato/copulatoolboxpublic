# README #

### What is this repository for? ###

* Repository contains a copula toolbox for Matlab. It's a poor man's version of Andrew Patton's (available at: http://public.econ.duke.edu/~ap172/code.html).
* Version: 0.1

### Some helpful git syntax: ###

When user is in local repository directory:

* git add \*.m   		    		
	- add all matlab .m files to staging area
* git status 		   				
	- check status of repo
* git commit -m 'someMessageHere'	
	- issue commit w/ message
* git push origin master 			
	- send committed changes to master on Bitbucket 
* git add -u						
	- automatically stage tracked files -- including deleting the previously tracked files
* git ls-files --deleted            
	- display locally deleted files 
* git ls-tree -r --name-only COMMIT
	- display all files in branch (where instead of COMMIT there can be BRANCH)
* git diff branchA:fileA branchB:fileB 
	- compare different files on different branches
* git diff branchA branchB file
	- compare same files across on branches
* git checkout -b branchB
	- create a new branch
* git push -u origin branchB:branchB
	- push new branch upstream
* pulling (incl. merge)
	- git pull --all
	- use git fetch if you want to merge manually after
	- More info:
		+ https://stackoverflow.com/questions/292357/what-is-the-difference-between-git-pull-and-git-fetch
		+ https://confluence.atlassian.com/bitbucket/pull-changes-from-your-git-repository-on-bitbucket-cloud-750395754.html

### How do I get set up? ###

* For instructions on setting up Git repository on local machine and pushing local files to Bitbucket see: 
	- https://confluence.atlassian.com/bitbucket/copy-your-git-repository-and-add-files-746520876.html 
* Note that MATLAB can do source control for you (as opposed to using terminal). See, ex.:
	- https://blogs.mathworks.com/community/2014/10/20/matlab-and-git/

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Dirk