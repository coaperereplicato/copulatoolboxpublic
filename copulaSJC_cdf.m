function cdf = copulaSJC_cdf(u, v, tauU, tauL, symmetric)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function computes distribution function of the symmetric Joe-Clayton copula.
% Args:
%   - u = N * 1 vector of probabiltiy transformed data (can be 1 * 1 or 1 *
%   N)
%   - v = N * 1 vector of probabiltiy transformed data ("                ")
%   - tauU = k * 1 vector of upper tail dependence parameters (can be 1 * 1
%   or 1 * k)
%   - tauL = k * 1 vector of lower tail dependence parameters ("         ")
%   - symmetric = (optional) logical, true = Symmetric JC, false = standard 
%   JC. If unspecified, symmetric JC is used.
% Output:
%   - pdf = N * 1 or k * 1 vector of density values
% Nb:
%   - For a reference see Patton, A.J., 2006, Modelling Asymmetric Exchange 
%   Rate Dependence, International Economic Review, 47(2), 527-556.
%   Patton's code is available from his website http://public.econ.duke.edu/~ap172/ 
%   - In particular see fn sym_jc_cdf()
%   - {u, v} can be vectors and {tauU, taul} scalars or vice-versa, but not
%   both
%   - dom(tauU) = dom(tauL) = (0,1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check dimensions ({u, v} scalars and {tauU, taul} vectors or vice versa, but not both)
copulaSJC_checkDimensions(u, v, tauU, tauL);
    
% Transformed parameters
k = @(tau) 1 ./ log2(2 - tau);
g = @(tau) - 1 ./ log2(tau);

% Joe-Clayton
JC = @(u, v, k, g) (1 - (1 - ((1 - (1 - u).^k).^(- g) + (1 - (1 - v).^k).^(- g) - 1).^(- 1 ./ g)).^(1 ./ k));

% Output desired copula
if nargin == 5 && ~symmetric
    % Joe-Clayton
    cdf = JC(u, v, k(tauU), g(tauL));
else
   % Symmetric Joe-Clayton
    cdf = (JC(u, v, k(tauU), g(tauL)) + JC(1 - u, 1 - v, k(tauL), g(tauU)) + u + v - 1) / 2;    % note switching of tauU & tauL, along with u -> 1 - u and similarly for v
end