function qDep = quantileDep(U, V, q)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function computes quantile dependence function as described in Patton
% (2012, s. 2.1.2). In particular, 
%   - upper quantile dependence = Pr(U_1 > q, U_2 > q) / (1 - q), q \in
%   [0.5, 1)
%   - lower quantile dependence = Pr(U_1 <= q, U_2 <= q) / q, q \in (0, 
%   0.5]
% Args:
%   - U = N * 1 vector of scalar data points
%   - V = N * 1 vector of scalar data points
%   - q = k * 1 vector of quantiles
% Output:
%   - qDep = k * 1 vector of quantile dependence evaluated at each quantile
%   in q
% Nb:
%   - Supp(q) = (0,1) since the input random variables probability
%   integral transform variables (of the original variables), i.e.
%   their domain is the unit cube.
%   - In Patton's code: (i) q are treated as percentiles and quantiles in 
%   the definition only quantiles are used. If U & V are perfectly uniform 
%   then it should not matter. See code for commented out alternative
%   implementation. And (ii) his 'upper' implementation assumes that Pr(U
%   <= q, V > q) = Pr(U > q, V <= q) = q.
%   - cf. Patton (2012, s. 2.1.2) and assoicated matlab fn available on his
%   website
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get dimensions
N = size(U, 1);
k = size(q, 1);
if N ~= size(V, 1)
    error('input vectors should have same length')
end
if k == 1 && size(q, 2) > 1
    k = size(q, 2); % handle case where q entered as a row vector 
end

% Compute statistic for each quantile in q
qDep = NaN(k, 1);
for ii = 1:1:k
    % lower 
    if q(ii) <= 0.5
        %qDep(ii) = sum((U <= q(ii)) .* (V <= q(ii))) / q(ii) / N;                           % q used only as quantile
        qDep(ii) = sum((U <= quantile(U, q(ii))) .* (V <= quantile(V, q(ii)))) / q(ii) / N; % q used as percentile, too
    % upper
    else
        %qDep(ii) = sum((U > q(ii)) .* (V > q(ii))) / (1 - q(ii)) / N;                           % q used only as quantile
        qDep(ii) = sum((U > quantile(U, q(ii))) .* (V > quantile(V, q(ii)))) / (1 - q(ii)) / N; % q used only as quantile  
    end
end
