function garchPlot(numPlots, plotData, yLabels, titles, subPlotTF, pd)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function plots conditional standard deviations, residuals and QQ plot.
% Args:
%   - numPlots: integer number of plots incl. QQ plot
%   - plotData: numPlots * 1 cell of plot data; each cell contains matrix
%   w/ two colums, 1st col = x; 2nd col = y
%   - yLabels: (numPlots - 1) * 1 cell of strings; labels for all plots 
%   excl. QQ plot
%   - titles: see 'yLabels'
%   - subPlotTF: logical; 1 => single figure; 0 => multiple figures
%   - pd: probability distribution object (theoretical distribtion of 
%   residuals for QQ plot)
% Output:
%   - n/a
% Ex.:
%   - see main.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;
for kk = 1:numPlots
    if subPlotTF == 0 && kk > 1
        figure; % new figure for each plot if not using subplot (skip 1st figure b/c it is outside for loop)
    end
    if kk == 3 % for qq plot, pass probabiltiy distribution
        if subPlotTF == 1
            subplot(numPlots, 1, kk);
        end
        qqplot(plotData{kk}(:, 1), pd);           
    else
        if subPlotTF == 1
            subplot(numPlots, 1, kk);
        end
        plot(plotData{kk}(:, 1), plotData{kk}(:, 2));
        ylabel(yLabels{kk});
        datetick('x', 'mmm-yy')
        axis tight
    end
    title(titles{kk});
end