function [x, fval] = bisection(fn, a, b)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function finds root of user function fn based on the bisection method.
% Args:
%   - fn = function HANDLE to a scalar function of one variable, x. (If
%   passing, say myFun.m syntax is bisectFind(@myFun, a, b).)
%   - a = scalar lower bound
%   - b = scalar upper bound
% Output:
%   - x = root
%   - fval = fn(x)
% Nb:
%   - Algorithm from wikipedia psudo code available at
%   https://en.wikipedia.org/wiki/Bisection_method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Defaults
maxIter = 100;
tol = eps / 2;% sqrt(eps);

% Algorithm (cf. https://en.wikipedia.org/wiki/Bisection_method)
count = 1;
while count < maxIter + 1               % avoid infinite loop
    c = (a + b) / 2;                    % new midpoint    
    if fn(c) == 0 || (b - a) / 2 < tol  % exit conditions
        %%%%%%%%%%%%%%%%%%%%% Debugging %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %         disp('exit conditions reached')
        %         if fn(c) == 0
        %             disp('exit reason: fn(c) == 0')
        %             fprintf('c: %.4e, fn(c): %.4e\n', c, fn(c));
        %         end
        %         if (b - a) / 2 < tol
        %             disp('exit reason: (b - a) / 2 < tol')
        %             fprintf('a: %.4e, b: %.4e, (b - a) / 2: %.4e, tol: %.4e\n', a, b, (b - a) / 2, tol);
        %         end
        %%%%%%%%%%%%%%%%%%%% END Debugging %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        x = c;
        fval = fn(x);
        if abs(fval) > sqrt(2)
            warning('check root existence; fval ~= 0')
        end
        return
    end
    if sign(fn(c)) == sign(fn(a))       % update domain depending on fn sign
        a = c;
    else
        b = c;
    end   
    count = count + 1;
end
error('bisection failed: maximum iteration (%d) reached without convergence', maxIter)
