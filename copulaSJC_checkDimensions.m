function copulaSJC_checkDimensions(u, v, tauU, tauL)

% Check dimensions
if sum(size(u) == size(v)) ~= 2
    error('u and v should have the same dimensions')
elseif sum(size(tauU) == size(tauL)) ~= 2
    error('tauU and tauL should have the same dimensions')
elseif max(size(u)) > 1 &&  max(size(tauU)) > 1
    error('{u, v} can be vectors and {tauU, taul} scalars or vice-versa, but not both')
end