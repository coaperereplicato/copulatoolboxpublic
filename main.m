%% Create or load in dataset
clear all; close all; clc

% Paths for source data and clean data file saving
fName = 'cleanedData.mat';
cleanDataFile = strcat(pwd, '\', fName);

% Create cleaned dataset if one doesn't already exist
% Nb:
%   - assumes dates are ascending order
%   - computes log returns
if exist(cleanDataFile, 'file') == 2
    load(cleanDataFile)
    fprintf('Cleaned dataset already exists & is now loaded.\n')
else
    % Read data
    dataEquity = readtable('GSPC.csv'); %, 'Format', '%s%f','Delimiter', ',', 'Headerlines', 1, 'ReadVariableNames', true);
    dataREIT = readtable('WILLREITIND.csv');

    % Convert string prices to double
    dataREIT.WILLREITIND = str2double(dataREIT.WILLREITIND);

    % Convert dates
    dataEquity.DATE = datenum(dataEquity.DATE);
    dataREIT.DATE = datenum(dataREIT.DATE);

    % Dates for data analysis
    startDate = max(min(dataEquity.DATE), min(dataREIT.DATE));
    endDate = min(max(dataEquity.DATE), max(dataREIT.DATE));
    tempDates = intersect(dataEquity.DATE,dataREIT.DATE);
    dates = tempDates(tempDates > startDate & tempDates < endDate);

    % Print sample info
    clc
    fprintf('\n\nStart date: %s \nFinish date: %s \n', datestr(dates(1)), datestr(dates(end)));
    fprintf('Number obs: %d \n\n', length(dates(end)));

    % Trim data to dates above
    dataREIT = dataREIT(ismember(dataREIT.DATE, dates), :);
    dataEquity = dataEquity(ismember(dataEquity.DATE, dates), :);

    % Merge data
    dataJoint = join(dataREIT, dataEquity, 'Keys','DATE');

    % Log return
    logRetDateAsc = @(price) log(price(2:end) ./ price(1:end - 1));

    % Dataset
    datesAsc = dataJoint.DATE(2:end);
    data = table(datesAsc, logRetDateAsc(dataJoint.AdjClose), logRetDateAsc(dataJoint.WILLREITIND), ...
        'VariableNames', {'date', 'retEquity', 'retREIT'});
    
    % Save cleaned data
    save(cleanDataFile, 'data');
end
    
%% Raw data plots (Optional)
close all;

% Return scatterplot
figure;
scatter(data.retEquity, data.retREIT)
xlabel('GSPC return')
ylabel('WILLREITIND return')
axis tight

% QQ plot
figure;
qqplot(data.retEquity, data.retREIT)
xlabel('GSPC')
ylabel('WILLREITIND')
axis tight

% Empirical density (log highlights effect at tails)
ub = max(max(data.retEquity), max(data.retREIT));
lb = min(min(data.retEquity), min(data.retREIT));
multiplier = 1.1;
numPts = 400;
pts = linspace(multiplier * lb, multiplier * ub, numPts);

figure;
hold on
plot(pts, log(ksdensity(data.retEquity, pts)))
plot(pts, log(ksdensity(data.retREIT, pts)), '-.' )
hold off
xlabel('return')
ylabel('log density')
legend('GSPC', 'WILLREITIND')

% Return timeseries (nb. different scales)
figure;
subplot(2,1,1);
plot(data.date, data.retEquity)
title('GSPC return')
datetick('x', 'mmm-yy')
axis tight

subplot(2,1,2);
plot(data.date, data.retREIT)
title('WILLREITIND return')
datetick('x', 'mmm-yy')
axis tight

% Squared return timeseries
figure;
subplot(2, 1, 1);
plot(data.date, data.retEquity .^ 2)
title('GSPC return')
datetick('x', 'mmm-yy')
axis tight

subplot(2, 1, 2);
plot(data.date, data.retREIT .^ 2)
title('WILLREITIND return')
datetick('x', 'mmm-yy')
axis tight

% ACF
figure;
subplot(2, 1, 1);
autocorr(data.retEquity)
title('GSPC return')

subplot(2, 1, 2);
autocorr(data.retREIT)
title('WILLREITIND return')

%% Naive correlation analysis (Optional)
% Fit bivariate normal to data (nb: closed form MLE estimates for bivariate
% normal; same as sample mean & covariance estimators)
ret = [data.retEquity,  data.retREIT];
muhat = mean(ret)';                              % sample mean
x = ret - muhat';                                % excess returns
sigmahat =(x' * x) / size(x, 1);                 % sample covariance
invSqrtDiag = @(covMat) minv(sqrt(eye(size(covMat, 1)) .* covMat));  % for correlation
rhohat =  invSqrtDiag(sigmahat) * sigmahat *  invSqrtDiag(sigmahat); % sample correlation
clear ret x invSqrtDiag
% Or simply: [stdhat, rhohat] = cov2corr(cov(ret));


% "Exceedance correlations" a la Longin & Solnik (2001) and Ang & Chen
% (2002); see also Patton (p. 138, 2004)

% Specify choice of real data vs simulated data 
mvnSim = true;
nSim = 2e6; % does not like orders of magnitude > 1e4 if non-parameteric ranks are used 

% Specify desired percentiles (for demarcating tails)
nPts = 200;                           % number of points in plot
pctiles = linspace(0.05, 0.95, nPts); % = [0.025, 0.05, 0.10, 0.50 - eps, 0.50 + eps, 0.90, 0.95, 0.975];

% Realised or simulated data
if mvnSim == true
    data2 = array2table(mvnrnd(muhat, sigmahat, nSim), 'VariableNames', ...
        data.Properties.VariableNames(2:end));
else
    data2 = data;
    data2.date = [];
end

% Compute exceedance correlations
Tsim = exceedanceCorr(table2array(data2(:, 1:end)), pctiles, false);
Tsim.Properties.VariableNames{2} = 'qntileEquity';
Tsim.Properties.VariableNames{3} = 'qntileREIT';

% Compute exceedance correlations (realised data)
Thist = exceedanceCorr(table2array(data(:, 2:end)), pctiles, false);
Thist.Properties.VariableNames{2} = 'qntileEquity';
Thist.Properties.VariableNames{3} = 'qntileREIT';

% Save
fNameCorrTables = {'corrTableSim.mat', 'corrTableHist.mat'};
corrTablesPath = strcat(pwd, '\', fNameCorrTables);
save(corrTablesPath{1}, 'Tsim');
save(corrTablesPath{2}, 'Thist');

% Load
if exist(corrTablesPath{1}, 'file') == 2 && exist(corrTablesPath{2}, 'file') == 2
    load(corrTablesPath{1})
    load(corrTablesPath{2})
    fprintf('Simulated already exists & is now loaded.\n')
else
    disp('file(s) not available')
end

% Plot

figure;
scatter(Thist.pctile, Thist.corr)
xlabel('percentile')
ylabel('correlation')

figure;
hold on
yyaxis left
scatter(Tsim.pctile, Tsim.corr, 'x')
yyaxis right
scatter(Thist.pctile, Thist.corr)
hold off
xlabel('percentile')
ylabel('correlation')


figure;
hold on
scatter(Thist.pctile, Thist.corr)
scatter(Tsim.pctile, Tsim.corr, 'x')
hold off
xlabel('percentile')
ylabel('exceedance correlation')
legend('Empirical', 'Simulated', 'Location', 'SouthEast')

clear pctiles

%% Marginal distribution fitting

plotACF = true;

% (P)ACF
if plotACF == 1
    figure;
    subplot(2, 1, 1);
    autocorr(data.retEquity)
    title('GSPC return')
    subplot(2, 1, 2);
    parcorr(data.retREIT)
    title('GSPC return')

    figure;
    subplot(2, 1, 1);
    autocorr(data.retREIT)
    title('WILLREITIND return')
    subplot(2, 1, 2);
    parcorr(data.retREIT)
    title('WILLREITIND return')
end

% Documentation: 
%   http://au.mathworks.com/help/econ/arima-class.htm
%   http://au.mathworks.com/help/econ/arima.estimate.html
%   https://au.mathworks.com/help/econ/specify-the-innovation-distribution.html
%   http://au.mathworks.com/help/econ/arima.infer.html
%   http://au.mathworks.com/help/stats/makedist.html
%   http://au.mathworks.com/help/stats/prob.tlocationscaledistribution.html
%   https://au.mathworks.com/help/econ/aicbic.html
%   https://en.wikipedia.org/wiki/Partial_autocorrelation_function
%%%%%%%%%%%%%%% Bug %%%%%%%%%%%%%%%
%   https://au.mathworks.com/matlabcentral/answers/270449-estimate-arma-1-1-using-estimate-parameter-ar-1-is-missing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Estimate a set of AMRA models for marginal return distributions
maxAR = 5;
maxMA = 5;
arLag = 0:maxAR;
maLag = 0:maxMA;
numMargins = length(data.Properties.VariableNames) - 1;                    % # of marginals to be fitted (exclude date vector)
modelInfo = struct('seriesName', [], 'lagAR', [],'lagMA', [], ...
    'EstMdl', [], 'EstParamCov', [], 'logL', [], 'aic', [], 'bic', []);    % data structure for saving estimated model
marginalModels = repmat(modelInfo, (1 + maxAR) * (1 + maxMA), numMargins); % row = variable; col = unique model (based of AR/MA combinations)
for k = 1:numMargins                                                       % estimate models over all ARMA combinations
    for i = 1:length(arLag)
        for j = 1:length(maLag)
            rowIdx = (i - 1) * length(maLag) + j;
            
            % Estimate arma model
            [EstMdl, EstParamCov, logL, info] = estimate(arima(arLag(i), 0, maLag(j)), data.(1 + k));
            
            % Get performace metrics
            numParam = length(info.X);
            numObs = length(data.(1 + k));
            [aic, bic] = aicbic(logL, numParam, numObs);
            
            % Push model estimation data to user defined structure
            marginalModels(rowIdx, k).seriesName = data.Properties.VariableNames{1 + k};
            marginalModels(rowIdx, k).lagAR = arLag(i);
            marginalModels(rowIdx, k).lagMA = maLag(j);        
            marginalModels(rowIdx, k).logL = logL;
            marginalModels(rowIdx, k).EstMdl = EstMdl;
            marginalModels(rowIdx, k).EstParamCov = EstParamCov;
            marginalModels(rowIdx, k).aic = aic;
            marginalModels(rowIdx, k).bic = bic;

        end
    end
end
clear rowIdx numParam aic bic i j k logL EstMdl EstParamCov

% Reshape & output performance metrics (log likelihood, aic, bic)
for i = 1:numMargins
    armaPerformanceMap(marginalModels(:, i), maxAR, maxMA, 'bic', true, data.Properties.VariableNames{i + 1})
end

% Get an estimated model (specify AR & MA lags + colIdx for desired data
% series
Mtemp = reshape(1:(1 + maxAR) * (1 + maxMA), maxMA + 1, maxAR + 1);
getEstMdl = @(lagAR, lagMA, colIdx) marginalModels(Mtemp(lagMA + 1, lagAR + 1), colIdx);

% Output info on desried model
EstMdlTemp = getEstMdl(3, 4, 1); % Ex. ARMA(3,4) for retEquity
print(EstMdlTemp.EstMdl, EstMdlTemp.EstParamCov)

EstMdlTemp = getEstMdl(3, 2, 1); 
print(EstMdlTemp.EstMdl, EstMdlTemp.EstParamCov)
EstMdlTemp = getEstMdl(2, 2, 1); 
print(EstMdlTemp.EstMdl, EstMdlTemp.EstParamCov)

%% Specfic marginal distribution fits

% Referenes:
% - GJR model (incl. analytical form) https://au.mathworks.com/help/econ/what-is-a-conditional-variance-model.html#bs7hrzn
% - infer for arima model (w/ possibly time varying errors) http://au.mathworks.com/help/econ/arima.infer.html
% - example of implementation w/ ARMA GARCH model https://au.mathworks.com/help/econ/fit-a-composite-conditional-mean-and-variance-model-to-nasdaq-returns.html


% Bivariate data
numMargins = length(data.Properties.VariableNames) - 1; 

% Specify marginal models
retMdl = {arima(1, 0 ,1), arima(1, 0 ,1)};
varMdl = {gjr('GARCHLags', 1, 'ARCHLags', 1, 'LeverageLags', 1), ...
    gjr('GARCHLags', 1, 'ARCHLags', 1, 'LeverageLags', 1)}; % or (e)garch()
varMdl{1}.Distribution = 't';
varMdl{2}.Distribution = 't';
for i = 1:numMargins  
    retMdl{i}.Variance = varMdl{i};
end

% Estimate (no pre-sampling implemented)
estMdl = cell(1, numMargins);
T = cell(1, numMargins);
Ttemp = array2table(NaN(size(data, 1), 2), 'VariableNames', {'error', 'condVar'});
for i = 1:numMargins   
    fprintf('\n%%------------------- %s ------------------- %%\n', data.Properties.VariableNames{i + 1});
    T{i} = Ttemp;
    estMdl{i} = estimate(retMdl{i}, data.(1 + i));
    [T{i}.error, T{i}.condVar, logL] =  infer(estMdl{i}, data.(1 + i));  
end
clear Ttemp logL 

% Manually check MATLAB fitted residuals (nb: code does not accommodate
% higher order lags > 1 in ARMA and GARCH models at present)
studentTvar = @(nu) nu ./ (nu - 2);         % variance of student-t distribution w/ nu > 2 d.o.f.
for i = 1:numMargins  
    % Compute conditional mean (dropping 1st observation b/c model is a lag order 1)
    tempData = data.(1 + i);
    condMean = estMdl{i}.Constant + estMdl{i}.AR{:} * tempData(1:end - 1) + estMdl{i}.MA{:} *  T{i}.error(1:end - 1); % nb: date in asc order
    
    % Computed residuals manually (realised return less conditional mean all divided by conditional std deviation)
    trimData = data.(1 + i);
    trimData = trimData(2:end);
    myResid = (trimData - condMean) ./ sqrt(T{i}.condVar(2:end));
    
    % For student-t garch errors
    if estMdl{i}.Distribution.Name == 't'
        % nb: myResid should be SWN(0,1) and ~generalised-t(nu, mu = 0, sigma^2 = (nu - 2) / nu)); alt., myResid ./ sqrt((nu - 2) / nu)) should be student-t(nu)
        distMyResid = fitdist(myResid, 'tLocationScale');                        % Fit generalsied-t to residuals
        varMyResidFit = sqrt(distMyResid.sigma^2 * studentTvar(distMyResid.nu)); % Compute total variance based on fitted dist. (should approximately equal one)
        fprintf('\n%%------------------- %s ------------------- %%\n', data.Properties.VariableNames{i + 1});
        fprintf('> mean and std dev of residuals: %.4f and %.4f, respectively \n> residual overall variance w/ fitted generalised-t dist: %.4f \n> GARCH fit nu: %.4f, residual fit nu: %.4f \n', ...
            mean(myResid), std(myResid), varMyResidFit, estMdl{i}.Distribution.DoF, distMyResid.nu)
    end
    
    % Check correspondence w/ Matlab built in method
    matlabResid = T{i}.error ./ sqrt(T{i}.condVar);
    matlabResid = matlabResid(2:end);
    distResid = myResid - matlabResid;
    if sum(abs(distResid)) > 1e-10
        % Plot for info if there are significant deviations
        figure; plot(abs(myResid - matlabResid))
        axis tight
        fprintf('sum of absolute distances b/w manual & matlab residuals: %E', sum(abs(myResid - matlabResid)))
    end
end
clear condMean trimData myResid tempData distResid

% Compute residuals
for i = 1:numMargins  
    T{i}.resid = T{i}.error ./ sqrt(T{i}.condVar); % manual checking in script below verifies why this formula is correct (cf. student-t case, in particular)
end 

% Plot conditional std & residuals
pd = cell(1, numMargins);
for i = 1:numMargins
    
    % Theoretical residual distributions 
    if strcmp(varMdl{i}.Distribution.Name, 't') == 1
        tempNu = estMdl{i}.Variance.Distribution.DoF;
        tempSigma = sqrt((tempNu - 2) / tempNu);         % nb: myResid should be SWN(0,1) and ~generalised-t(nu, mu = 0, sigma^2 = (nu - 2) / nu))
        pd{i} = makedist('tLocationScale', 'mu', 0, 'sigma', tempSigma, 'nu', tempNu); 
    elseif strcmp(varMdl{i}.Distribution.Name, 'Gaussian') == 1
        pd{i} = makedist('Normal', 'mu', 0, 'sigma', 1); 
    else
    end
    
    % Plot {data, lables, titles}
    numPlots = 3;
    yLabels = {'\sigma_{t} (%)', '\epsilon_{t}'};
    plotData = {[data.date, 100 * sqrt(T{i}.condVar)], ... % conditional std dev
        [data.date, T{i}.resid], ...                       % residuals
        T{i}.resid};                                       % qq plot residuals
    titles = {sprintf('%s', data.Properties.VariableNames{i + 1}), ...
        sprintf('%s', data.Properties.VariableNames{i + 1}), ...
        sprintf('QQ plot: %s', data.Properties.VariableNames{i + 1})};
   
    % Plot residuals & conditional standard deviations
    garchPlot(numPlots, plotData, yLabels, titles, true, pd{i})
    %garchPlot(numPlots, plotData, yLabels, titles, false, pd{i})
    clear numPlots ylabels plotData titles
   
end
clear tempNu tempSigma i

%% Preliminary residual analysis

% Check for serial correlation in residuals
for i = 1:numMargins
    [h, pValue] = lbqtest(T{i}.resid);
    if pValue < 0.1
        fprintf('investigate serial correlation in %s residuals (Ljung-Box test statistic pval = %.2f)\n', ...
            data.Properties.VariableNames{i + 1}, pValue)
    end
end
clear h pValue

% Rename residuals / filtered data for convenience
x = T{1}.resid;  % equity
y = T{2}.resid;  % REIT

% Residual scatter
figure;
scatterhist(x, y)
title('residuals')
xlabel(data.Properties.VariableNames{1 + 1})
ylabel(data.Properties.VariableNames{1 + 2})

% Exceedance correlations (EC) for raw data (returns) & residuals
nPts = 200;                           % number of points in function domain
pctiles = linspace(0.05, 0.95, nPts); % specify desired percentiles (for demarcating tails)
TplotRet = exceedanceCorr([data.retEquity, data.retREIT], pctiles, true);
TplotRes = exceedanceCorr([x, y], pctiles, true);
varNames = {'Equity', 'REIT'};

% Individual EC plots (auto y-axis)
exceedanceCorrPlot(TplotRet, 'method', 'Pearson', 'varNames', strcat('ret', varNames))
exceedanceCorrPlot(TplotRes, 'method', 'Pearson', 'varNames', strcat('res', varNames))

% Joint EC plot (manual y-axis + title adjustments)
ylim = [0.2, 0.7];
method = 'Pearson';
subplotTitle = sprintf('Exceedance (%s) Correlations', method);
figure;
subplot(1, 2, 1)
exceedanceCorrPlot(TplotRet, 'method', 'Pearson', ...
    'varNames', strcat('ret', varNames), 'yaxis', ylim, 'subplot', true)
subplot(1, 2, 2)
exceedanceCorrPlot(TplotRes, 'method', 'Pearson', ...
    'varNames', strcat('res', varNames), 'yaxis', ylim, 'subplot', true)
suptitle(subplotTitle)
clear TplotRet TplotRes varNames ylim method subplotTitle


% Empirical cdf
% Nb: 
%   - Theoretical justificaton for kernel density usage in convergence of 
%   empirical copula to true copula provided in Fermanian, Radulovi? and 
%   Wegkamp (2004) 'Weak convergence of empirical copula processes'. See
%   also discussion in Patton (2012, s. 2.1).
kernel = true;
if kernel == true
    u = ksdensity(x, x, 'function','cdf');
    v = ksdensity(y, y, 'function','cdf');
else
    u = empiricalCDF(x);
    v = empiricalCDF(y);
end

% Probabiltiy transform data scatter
figure; 
scatterhist(u, v)
title('transformed data')
xlabel(sprintf('F(%s)', data.Properties.VariableNames{1 + 1}))
ylabel(sprintf('F(%s)', data.Properties.VariableNames{1 + 2}))

% Exceedance correlation w/ probability integral transform of residuals
exceedanceCorrPlot(exceedanceCorr([u, v], pctiles, true), 'varNames', {'F(resEquity)', 'F(resREIT)'}) % using percentiles as specified above
clear pctiles nPts

% Quantile depednence (cf. Patton (2012, s 2.1.2) and function description)
% Nb: 
%   - Issue w/ reliance on iid for bootstrap approach (see intro to wiki
%   article). Also, relates to CI for copula fit (see Patton (2012,  s.
%   2.1)and references therein)
%   - run time w/ nSim = 150 approx 27s on 3.3GHz i5 6600
CIalpha = 0.10;
[q, qDep, qDepDiff, qDiff, qDepCI, qDepDiffCI] = fnCallWithTimer(6, @quantileDepWrapper, u, v, ...
    'qntleDiff', true, ...  % also compute upper minus lower quantile statistic
    'CIbounds', true, ...   % compute confidence interval bounds for statistics
    'CIalpha', CIalpha, ... % alpha for CI
    'nSim', 3);

% Quantile dependence plots
figure;
hold on
h = plot(q, qDep);
scatter(q, qDep)
h2 = plot(q, qDepCI, '-.');
hold off
legend('', 'data', sprintf('%0.f%% CI', 100 * (1- CIalpha)) )
ylabel('Quantile dependence')
xlabel('Quantile')
%axis([0, 1, 0.3, 0.9])
h2(2).Color = h2(1).Color;
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off'); % https://au.mathworks.com/matlabcentral/answers/406-how-do-i-skip-items-in-a-legend

figure;
hold on
h = plot(qDiff, qDepDiff);
scatter(qDiff, qDepDiff)
h2 = plot(qDiff, qDepDiffCI, '-.');
hold off
legend('', 'data', sprintf('%0.f%% CI', 100 * (1- CIalpha)) )
ylabel('Upper minus lower')
xlabel('Quantile')
h2(2).Color = h2(1).Color;
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

clear CIalpha h h2

%% Fit symmetric Joe-Clayton copula to residuals (using my pdf & likelihood functions)

% MLE for symmetric JC copula
singleOptimisation = true;             % single vs multiple starting points
if singleOptimisation == true
    [thetaMLE, fval] = copulaSJC_calibrate(u, v, 'copula', 'SymmetricJoeClayton');
else
    % Set optimisation function as a function of starting point only
    optFn = @(theta0) copulaSJC_calibrate(u, v, 'copula', 'SymmetricJoeClayton', 'theta0', theta0);
        
    % Set of initial starting points (nb: too {tauU, tauL} too close to
    % bounds will send parts of copula pdf to Inf)
    nStartPts = 4;                                                      % number of initial guesses for each parameter
    tempDom1 = linspace(0.2, 0.8, nStartPts);                           % param 1 guess domain 
    tempDom2 = linspace(0.2, 0.8, nStartPts);                           % param 2 guess domain
    [gridTempX, gridTempY] = meshgrid(tempDom1, tempDom2);              % parameter guess combinations 
    theta0 = [reshape(gridTempX,  [], 1), reshape(gridTempY,  [], 1)];  % parameter guess remapped (row = combination, col = [tauU0, tauL0])
    numOpt = size(theta0, 1);                                           % total number of optimisations gsiven nStartPts
    
    % Preallocate for optimisations
    ThetaMLE = - 999 * ones(numOpt, 2);
    fval = - 999 * ones(numOpt, 1);
    
    % MLE
    for ii = 1:1:numOpt
        [temp, fval(ii)] = optFn(theta0(ii, :)');
        ThetaMLE(ii, :) = temp';
    end
    
    % Print MLE results
    ThetaMLE = array2table([theta0, ThetaMLE, fval], 'VariableNames', {'tauU0', 'tauL0', 'tauU_hat', 'tauL_hat', 'minuslogL'});
    disp(ThetaMLE)
end
clear objFn optFn temp ii temp

% Get fitted parameters
tauUhat = thetaMLE(1);
tauLhat = thetaMLE(2);

% Nb: diffrences in SJC fitted parameters due to use of kernel in obtaining
% empirical density
tauHatULwEmpiricalUV = [0.3420, 0.4330]; % no kernal density applied to residuals; used my own empirical density fn
tauHatULwKernelUV =  [0.3515,  0.4373];  % kernel used to get empirical density

%% Plot density & distribution functions

% Plot fitted copula (see copulaPlot3D.m documentation for more user specified options)
fittedSJCParams = {'tauU', tauUhat, 'tauL', tauLhat}; % need name value pair construction as nameValuePairParser.m is called in copulaPlot3D.m
paramInfoLocationContour = [0.15, 0.6, 0.3, 0.3];     % location for outputting parameter info on contour plots
copulaPlot3D('SymmetricJoeClayton', fittedSJCParams, 'plotType', 'cdf', 'contourPlot', false, 'showAnnotation', true)
copulaPlot3D('SymmetricJoeClayton', fittedSJCParams, 'plotType', 'pdf', 'contourPlot', false, 'showAnnotation', true)
copulaPlot3D('SymmetricJoeClayton', fittedSJCParams, 'plotType', 'cdf', 'contourPlot', true, 'numContours', 10, 'showAnnotation', true, 'annotationLocation', paramInfoLocationContour)
copulaPlot3D('SymmetricJoeClayton', fittedSJCParams, 'plotType', 'pdf', 'contourPlot', true, 'numContours', 50, 'showAnnotation', true, 'annotationLocation', paramInfoLocationContour)
clear fittedSJCParams paramInfoLocationContour

% Plot empirical pdf of fitted standardised residuals
tempLabels = {'\epsilon_X', '\epsilon_Y'};
tempTitle = 'Empirical pdf (standardised residuals)';
epdfPlot3D(x, y, 'plotLabels', tempLabels, 'plotTitle', tempTitle)
epdfPlot3D(x, y, 'plotLabels', tempLabels, 'plotTitle', tempTitle, 'contourPlot', true, 'numContours', 15);
clear tempLabels tempTitle

% Plot joint distribution of standardised residuals from ARMA-GARCH fit
%   - let (X, Y) denote the standardised resiudals
%   - we are after density fn: 
%       f_{XY}(x,y) = c(F_X(x), F_Y(y)) * f_X(x) * f_Y(y),
%   where c(.,.) is the copula density fn
%   - Methods:
%   (i) fully parametric: we use parametric marginal distribtuions for f_X
%   & f_Y along with F_X & F_Y which appear in the copula density, in
%   addition to the parametric SJC copula density c(.,.)
%   (ii) semi-parametric: we use empirical marginal disributions, with only
%   the copula being parametrically specified
residualSJC_pdf = cell(1, 2);
residualSJC_pdf{1} = @(x, y, tauU, tauL) ...          % parametric margins as specified in cell array containing std residual margin fits
    copulaSJC_pdf(cdf(pd{1}, x), cdf(pd{2}, y), tauU, tauL, true) ...
    .* pdf(pd{1}, x) .* pdf(pd{2}, y);
residualSJC_pdf{2} = @(x, y, tauU, tauL) ...          % empirical margins using normal kernel smoothing
    copulaSJC_pdf(ksdensity(x, x, 'Function', 'cdf'), ksdensity(y, y, 'Function', 'cdf'), tauU, tauL, true) ...
    .* ksdensity(x, x, 'Function', 'pdf') .* ksdensity(y, y, 'Function', 'pdf');  
tempTitles = strcat('Fitted SJC copula ', {' '}, {'(parametric margins)', '(empirical margins)'});
tempLabels = {'\epsilon_X', '\epsilon_Y'};
numSteps = 100;                     % steps in grid
pctileRange = [0.005; 0.995];       % truncation of data for grid
empiricalDom = @(data) linspace(quantile(data, pctileRange(1)), ...
    quantile(data, pctileRange(2)), numSteps)';               % domain for grid
[xGrid, yGrid] = meshgrid(empiricalDom(x), empiricalDom(y));  % generate grid
plotAcutalData = false;                                       % mark actual data points
plotContours = false;                                          % plot pdf w/ mesh or pdf w/ contours
numContours = 15;                                             % number of contour lines in pdf plot
for ii = 1:1:length(residualSJC_pdf)
    zTemp =  residualSJC_pdf{ii}(x, y, tauUhat, tauLhat);      % actual z values
    zInterpolate = griddata(x, y, zTemp, xGrid, yGrid);       % interpolated z values
    figure;
    if plotContours
        contour(xGrid, yGrid, zInterpolate, numContours)  
    else
        surf(xGrid, yGrid, zInterpolate)  
    end                 
    if plotAcutalData
        hold on
        plot3(x, y, zTemp, 'o')                               % mark actual data points
        hold off
    end
    title(tempTitles{ii})
    xlabel(tempLabels{1})
    ylabel(tempLabels{2})
    axis tight
end
clear residualSJC_pdf numSteps pctileRange empiricalDom xGrid yGrid ii zTemp zInterpolate plotAcutalData numContours

% notes:
%   - parameteric margins much cleaner
%   - empricial margin pdf appears to be rotated counterclockwise about the
%   pdf mode relative to the parametric margin pdf; we deduce that a
%   similar rotation observed in fully empirical PDF vs fully parametric
%   PDF is due to MARGINS and not due to COPULA. [this is not correct: look @ rotation of actual pdfs]

%% Exceedence correlation & quantile dependence plots w/ simulated SJC

% Simulate from SJC copula w/ SJC fitted parameters 
largeSample = false;
if largeSample 
    % Create a large sample and save to sub-directory in pwd
    nSimTemp = 1e6;
    stripDecimalPlace = @(string) string(string ~= '.');
    fnameTemp = strcat(sprintf('nSim_%.0e', nSimTemp), '_Copula_SJC','_Params_', ...
        sprintf('tauL%s_tauU%s', stripDecimalPlace(num2str(tauLhat)),stripDecimalPlace(num2str(tauUhat))));
    save(strcat(pwd, '\simulations\',fnameTemp), 'rnd')
    rnd = fnCallWithTimer('', @copulaSJC_rnd, tauLhat, tauUhat, 'numSim', nSimTemp, 'useParallel', true);  % 62(s) [649.62(s)] for 1e5 [1e6] simulations using parallel (w/ existing pool) on 3.3GHz i5 6600
else 
    % Create a modest sample (no save)
    rnd = fnCallWithTimer('', @copulaSJC_rnd, tauLhat, tauUhat, 'numSim', 1e4, 'useParallel', false); % 20s fpr 1e4 (serial), 6s (parallel, w/ existing pool) on 3.3GHz i5 6600
end
clear largeSample nSimTemp

% Use empirical quantile function to get sample of standardised residuals (based off fitted SJC copula & empirical margins)
quantileEmp = @(x, u) quantile(x, u);
xSim1 = quantileEmp(x, rnd(:, 1));   % F_X^{-1}(uSim) = quantile(x, uSim)
ySim1 = quantileEmp(y, rnd(:, 2));   % F_Y^{-1}(vSim) = quantile(y, vSim)

% Use parametric quantile function to get sample of standardised residuals (based off fitted SJC copula & fitted margins)
xSim2 = icdf(pd{1},  rnd(:, 1));     % inverse cdf of probability distribution object (fitted student-t margin)
ySim2 = icdf(pd{2},  rnd(:, 2));

% Plot SJC simulated standardised residuals
figure;
hold on
scatter(xSim1, ySim1, 'o')
scatter(xSim2, ySim2, 'x')
legend({'empirical marign', 'parametric margin'}, 'Location', 'southeast')
title('Simulated std residuals (fitted SJC copula)')
xlabel('resEquity')
ylabel('resREIT')
hold off

% Exceedance correlations (EC) for simulated residuals data
varNames = {'Equity', 'REIT'};
nPtsSimPlot = 1e3;                              % number of points in function domain
pctilesSim = linspace(0.05, 0.95, nPtsSimPlot); % specify desired percentiles (for demarcating tails)
TplotSim1 = exceedanceCorr([xSim1, ySim1], pctilesSim, false);
TplotSim2 = exceedanceCorr([xSim2, ySim2], pctilesSim, false);
exceedanceCorrPlot(TplotSim1, 'method', 'Pearson', 'varNames', strcat('resSim', varNames), 'yaxis', [0.2, 0.7])
exceedanceCorrPlot(TplotSim2, 'method', 'Pearson', 'varNames', strcat('resSim', varNames), 'yaxis', [0.2, 0.7])
clear varNames nPtsSimPlot pctilesSim TplotSim1 TplotSim2



% Create a sparse domain for percentiles so that linear interpolation
% implicit in matlab plot function will smooth the noise in the simulated
% sample
pctilesSimSmooth = [linspace(0.05, 0.49, 5), 0.499 0.5, 0.501 linspace(0.51, 0.95, 5)];  % dense around median for sharp transition

% Exceedance correlations (EC) for transformed simulated residuals data
% using SJC copula
Tplot_uvSim = exceedanceCorr([rnd(:, 1), rnd(:, 2)], pctilesSimSmooth, false);
exceedanceCorrPlot(Tplot_uvSim, 'method', 'Pearson', 'varNames', strcat('uvSim', varNames), 'yaxis', [0.2, 0.7])

% Calibrate data to matlab's built in copulas
[copulaFit, copulaNames] = copulaBuitltin2D_calibrate(u, v);

% Simulate from calibrated built in copulas
numSim = 1e6;
rndBuiltin = fnCallWithTimer('', @copulaBuitltin2D_rnd, copulaFit, copulaNames, numSim);
clear numSim

% Exceedance correlations (EC) for transformed simulated residuals using
% matlab's built in copulas
Tplot_uvSim_builtinCopulas = cell(1, length(copulaNames));
tempLineStyle = {'-', '--', '-.'}; %':' also available but not v. legible
tempMarker = {'x', '+', '*', 'd', 's', 'p'};
tempMethod = 'Pearson';
figure;
hold on
for ii = 1:1:length(copulaNames)
    
    Tplot_uvSim_builtinCopulas{ii} = exceedanceCorr([rndBuiltin{ii}(:, 1), ...
        rndBuiltin{ii}(:, 2)], pctilesSimSmooth, false);
    h = exceedanceCorrPlot(Tplot_uvSim_builtinCopulas{ii}, 'method', tempMethod, ...
        'varNames', strcat('copula ', copulaNames), 'yaxis', [0.0, 1.0], ...
        'holdplot', true);
    
    if ii <= length(tempLineStyle)
        h.LineStyle = tempLineStyle{ii};
    else
        h.Marker = tempMarker{ii}; % switch to markers after line styles exhausted
    end
    
end
hold off
legend(copulaNames)
title(sprintf('Exceedence (%s) correlations', tempMethod)) % reset title
clear tempLineStyle tempMarker tempMethod h 


% Exceedance correlations (EC) for standardised residuals (raw data)
nPts = 200;                           % number of points in function domain
pctiles = linspace(0.05, 0.95, nPts); % specify desired percentiles (for demarcating tails)

TplotRes = exceedanceCorr([x, y], pctiles, true);
varNames = {'Equity', 'REIT'};
exceedanceCorrPlot(TplotRes, 'method', 'Pearson', 'varNames', strcat('res', varNames),'yaxis', [0.0, 1.0])


[tauLhat, tauUhat]
%%% ADD horizontal lines


copulaParams = {'tauU', 0.25, 'tauL', 0.30};
copula2D_rnd('SymmetricJoeClayton', copulaParams)
fnCallWithTimer('', @copula2D_rnd, 'SymmetricJoeClayton', copulaParams, 'numSim', 1, 'useParallel', true, 'seed', 0)

fnCallWithTimer('', @copula2D_rnd, 't', {'rho', 0.4, 'nu', 6}, 'numSim', 3, 'useParallel', false, 'seed', 0)




% cf. Figure 2, Patton 2004, which plots exceedance correlations for (U,V) with simulated {normal, rotated gumbel} copula

pctilesSimTails = [linspace(0.05, 0.49, 60), linspace(0.51, 0.95, 60)]';
TplotSim1_tails = exceedanceCorr([xSim1, ySim1], pctilesSimTails, false);
exceedanceCorrPlot(TplotSim1_tails, 'method', 'Pearson', 'varNames', strcat('resSim', varNames), 'yaxis', [0.2, 0.7])









% Exceedance correlations (EC) for raw data (returns) & residuals
nPts = 200;                           % number of points in function domain
pctiles = linspace(0.05, 0.95, nPts); % specify desired percentiles (for demarcating tails)

TplotRet = exceedanceCorr([data.retEquity, data.retREIT], pctiles, true);
TplotRes = exceedanceCorr([x, y], pctiles, true);
varNames = {'Equity', 'REIT'};

% Individual EC plots (auto y-axis)
exceedanceCorrPlot(TplotRet, 'method', 'Pearson', 'varNames', strcat('ret', varNames))
exceedanceCorrPlot(TplotRes, 'method', 'Pearson', 'varNames', strcat('res', varNames))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   SIMULATION TESTING   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rnd = copulaSJC_rnd(0.4330, 0.3420, 'numSim', length(u)); % simulate from SJC copula w/  SJC fitted parameters 
figure; scatter(rnd(:,1), rnd(:,2))
figure; scatter(u, v);

rndJC = fnCallWithTimer('', @copulaSJC_rnd, 0.9, 0.9, 'numSim', length(u));
rndSJC = fnCallWithTimer('', @copulaSJC_rnd, 0.9, 0.9, ...
    'numSim', length(u), ...
    'copula', 'SymmetricJoeClayton');
figure; scatter(rndJC(:,1), rndJC(:,2))
figure; scatter(rndSJC(:,1), rndSJC(:,2))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%% My to do list

% [DONE] Fit Patton's SJC copula 
% [DONE] Simulate from fitted SJC copula
%   - https://people.math.ethz.ch/~embrecht/ftp/copchapter.pdf p. 11
%   - reverse engineer Pattons code
% [DONE] Implement pdf and likelihood functions for (S)JC copula
% [DONE] Plot copula {pdf, cdf} incl. contour plots
% [DONE] Plot standardised residuals pdf (both parametric & empirical)
% Plot exceedance Correlation function from simulated SJC
% Plot Quantile Depednence function from simulated SJC
% Fit empirical copula (EC)(see analytical def. in Patton's paper)
% Comptue distance b/w SJC and EC

rhohat = copulafit('Gaussian',[u, v]);

r = copularnd('t',Rho,nu,1000);
u1 = r(:,1);
v1 = r(:,2);

% https://au.mathworks.com/help/stats/copulafit.html





%% To Do
figure;
scatter(T.pctile, T.corrSpearman)
figure;
scatter(T.pctile, T.corrKendall)

% To do:
%   - error bars in realised data correlations
%   - turn generation of data into a function
%   - plot with realised and simulated data for comparison
%   - do analysis again with fitted copula, cf. Patton (2004, Figure 2)

%figure;
%errorbar(x,y,err) %%%% put in 95% confidence bounds here

