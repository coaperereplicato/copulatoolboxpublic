function T = exceedanceCorr(data, pctiles, nonparamCorr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function computes exceedance correlations for bivariate data set. 
% In particular, the correlation of the two RVs over their respective
% quantiles (corresponding to a given percentile) is computed. If the
% percentile is under (resp. over) 50, the correlation pertains to data
% in [0, pctile] (resp. (pctile, 100]), i.e. tail correlations are 
% reported.
% Args:
%   - data = N x 2 matrix (for any natural N), each column contains an RV
%   - pctiles = vector of percentiles
%   - nonparamCorr = true | false, nonparameteric measures of correlation
% Output
%   - T = summary table of correlations
% Nb:
%   - for a definiton of exceedance correlation see ex. Patton 
%   (2004, p. 138)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
qntiles = quantile(data, pctiles);
T = array2table(zeros(length(pctiles), 10));
T.Properties.VariableNames = {'pctile', 'qntileX', 'qntileY', ...
'n', 'corr', 'pval', 'corrSpearman', 'pvalSpearman', 'corrKendall', 'pvalKendall'}; 
for i = 1:1:length(pctiles)
    if pctiles(i) < 0.5   % lower tail
        temp = data(data(:, 1) <= qntiles(i, 1) & data(:, 2) <= qntiles(i, 2), :);
    else                  % upper tail
        temp = data(data(:, 1) > qntiles(i, 1) & data(:, 2) > qntiles(i, 2), :);
    end
    if size(temp, 1) < 5
        disp('insufficient data for correlation at at least on percentile')
        T = [];
        return;
    end
    [rho, pval] = corr(temp(:, 1), temp(:, 2));
    T(i, 1:end - 4) = {pctiles(i), qntiles(i, 1), qntiles(i, 2), size(temp, 1), ...
        rho, pval};
    if nonparamCorr == true
        [rho2, pval2] = corr(temp(:, 1), temp(:, 2), 'type', 'Spearman');
        [rho3, pval3] = corr(temp(:, 1), temp(:, 2), 'type', 'Kendall');
        T(i, end - 3:end) = {rho2, pval2, rho3, pval3};
    else
    end
end
if nonparamCorr ~= true
     T(:, end - 3:end) = [];
end