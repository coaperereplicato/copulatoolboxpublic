function T = nameValuePairParser(nameValueCellArray)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function is a bare bones name-value parser. Used when Matlab's built in
% parser functionality is already being used in function signature and a
% secondary parser is requried, ex. function that has name-value pair 
% variable but also calls another function. (There is probably a smarter
% way to do this in a single step using built in functionality.)
% Args:
%   - nameValueCellArray = (numValues * 2) array containing name-value
%   pairs; ex. {'tauU', 0.2, 'tauL', 0.5}.
% Output:
%   - T = (1 * numValues) table; ex. T.tauU = 0.2
% Nb:
%   - for usage example, see, ex., copulaPlot3D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

numValues = length(nameValueCellArray) / 2;
paramNames = nameValueCellArray(1:2:(2 * numValues));
paramVals = nameValueCellArray(2:2:(2 * numValues + 1));
T = cell2table(paramVals, 'VariableNames', paramNames);