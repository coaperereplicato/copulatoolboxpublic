function epdfPlot3D(x, y, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function plots empirical bivariate pdf or corresponding contour plot. 
% Args:
%   - x = N * 1 vector of data from margin 1
%   - y = N * 1 vector of data from margin 2
%   - x = string containing copula name
%   - 'contourPlot' = (optional) logical; plot contour of pdf instead 
%   of pdf itself
%   - 'contourLevels' = (optional) vector of heights at which contours are
%   plotted [do not specify if using 'numContours' option]
%   - 'numContours' = (optional) scalar number of contour lines
%   - 'numSteps' = (optional) scalar number of steps in (square) domain
%   grid
%   - 'pctileRange' = (optional) vector of length 2 containing lower and
%   upper percentiles of data, respectively, used truncate plot domain
%   - 'plotLabels' = (optional) 1 * 2 cell array containing labels
%   specified as strings, ex. {'myXlabel', 'myYlabel'}
% Outputs:
%   - N/A
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create input parser object 
p = inputParser;

% Check if number is natural (for number of grid points and number of 
% contour lines checks)
isnatural = @(x) isscalar(x) && (mod(x, 1) == 0) && x > 0;   % natural number

% Default number of points in each domain
defaultGridSteps = 1e2;

% Default quantiles for grid
deafaultPctileRng = [0.005, 0.995];
checkPctileRng = @(x) sum(size(x)) == 3 ...                       % vector of length 2
                        && sum((x(:) > 0) .* (x(:) < 1)) == 2 ... % both percentiles in open interval (0,1)
                        && x(1) < x(2);                           % percentiles in increasing order
% Default contours
defaultContour = false;

% Default number of contour lines
defaultNumContours = 15;

% Default heights for countour plots
defaultLevels = NaN; % we will set default below based on data

% Default title
defaultTitle = NaN; % NaN corresponds to no title (see implementation below)

% Default x & y plot labels
defaultLabels = NaN;                                                  % NaN corresponds to no labels (see implementation below)
myStringTest = @(x) isstring(horzcat(x{:})) || ischar(horzcat(x{:})); % true if 'x' is a cell of strings or characters

% Check margins are vectors of the same length
checkMarginY = @(y) sum(size(y) == size(x)) == 2;

% Add inputs to scheme
addRequired(p, 'x', @isvector);
addRequired(p, 'y', checkMarginY);
addOptional(p, 'numSteps', defaultGridSteps, isnatural);
addOptional(p, 'contourPlot', defaultContour, @islogical);
addOptional(p, 'numContours', defaultNumContours, isnatural);
addOptional(p, 'contourLevels', defaultLevels, @isvector);
addOptional(p, 'pctileRange', deafaultPctileRng, checkPctileRng)
addOptional(p, 'plotLabels', defaultLabels, myStringTest);
addOptional(p, 'plotTitle', defaultTitle, @ischar);

% Parse (requiring exact matches)
p.KeepUnmatched = false;
parse(p, x, y, varargin{:})

% Get optional inputs
numSteps = p.Results.numSteps;
contourPlot = p.Results.contourPlot;
contourLevels = p.Results.contourLevels;
numContours = p.Results.numContours;
pctileRange = p.Results.pctileRange;
plotLabels = p.Results.plotLabels;
plotTitle = p.Results.plotTitle;

% Get domain grid based on percentiles 
empiricalDom = @(data) linspace(quantile(data, pctileRange(1)), quantile(data, pctileRange(2)), numSteps)';
xDom = empiricalDom(x);
yDom = empiricalDom(y);

% Make grid
[gridTempX, gridTempY] = meshgrid(xDom, yDom);
gridTemp = [gridTempX(:), gridTempY(:)];

% Empirical pdf
[zEmp, xi] = ksdensity([x, y], gridTemp); % Nb: gridTempX =  myShape(xi(:, 1)),  gridTempY =  myShape(xi(:, 2)) [sum(sum(gridTempX == myShape(xi(:,1)), 1)) == length(xi)]

% Reshape pdf function values for builtin matlab plotting functions
myShape = @(v) reshape(v, numSteps, numSteps);

% Plot
figure;
if ~contourPlot 
    surf(gridTempX, gridTempY, myShape(zEmp))
else
    % If contour levels specified use them, o/wise use (possibly user defined) number of contour levels  
    if isnan(contourLevels)
        contourParam = numContours;     % natural number
    else
        contourParam = contourLevels;   % linspace
    end
    contour(gridTempX, gridTempY, myShape(zEmp), contourParam)
end
axis tight

% Add plot labels if specified by user
try 
    isnan(plotLabels); % if plotLabels = {'myxlabel', 'myylabel'} then this statement is not valid and we move to catch statement
catch
    xlabel(plotLabels{1})
    ylabel(plotLabels{2})
end

% Add plot title if specified by user
if ischar(plotTitle)
    title(plotTitle)
end